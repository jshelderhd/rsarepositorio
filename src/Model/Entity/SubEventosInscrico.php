<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubEventosInscrico Entity
 *
 * @property int $evento_sub_eventos_id
 * @property int $inscricoes_id
 * @property bool $ativo
 *
 * @property \App\Model\Entity\EventosSubEvento $eventos_sub_evento
 * @property \App\Model\Entity\Inscrico $inscrico
 */
class SubEventosInscrico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'evento_sub_eventos_id' => false,
        'inscricoes_id' => false
    ];
}
