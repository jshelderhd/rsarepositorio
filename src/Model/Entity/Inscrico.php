<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inscrico Entity
 *
 * @property int $inscricoes_id
 * @property int $eventos_id
 * @property int $usuarios_id
 * @property bool $tipo_insc
 * @property \Cake\I18n\Time $data_inscricao
 * @property int $autor_inscricoes_id
 * @property bool $ativo
 *
 * @property \App\Model\Entity\Inscrico $inscrico
 * @property \App\Model\Entity\Evento $evento
 * @property \App\Model\Entity\Usuario $usuario
 * @property \App\Model\Entity\SubEvento[] $sub_eventos
 */
class Inscrico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'inscricoes_id' => false
    ];
}
