<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Submico Entity
 *
 * @property int $submicoes_id
 * @property string $titulo
 * @property int $inscricoes_id
 * @property int $cursos_id
 * @property bool $ativo
 *
 * @property \App\Model\Entity\Submico $submico
 * @property \App\Model\Entity\Inscrico $inscrico
 * @property \App\Model\Entity\Curso $curso
 */
class Submico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'submicoes_id' => false
    ];
}
