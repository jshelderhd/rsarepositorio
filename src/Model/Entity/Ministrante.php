<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ministrante Entity
 *
 * @property int $ministrante_id
 * @property string $nome_ministrante
 * @property string $foto_ministrante
 * @property string $sobre
 * @property int $Eventos_sub_eventos_id
 * @property bool $ativo
 *
 * @property \App\Model\Entity\Ministrante $ministrante
 * @property \App\Model\Entity\EventosSubEvento $eventos_sub_evento
 */
class Ministrante extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'ministrante_id' => false
    ];
}
