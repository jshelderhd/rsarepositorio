<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Instituico Entity
 *
 * @property int $instituicoes_id
 * @property string $nome
 * @property int $cursos_id
 * @property bool $ativo
 *
 * @property \App\Model\Entity\Instituico $instituico
 * @property \App\Model\Entity\Curso $curso
 */
class Instituico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'instituicoes_id' => false
    ];
}
