<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Modalidade Entity
 *
 * @property int $modalidades_id
 * @property string $nome
 * @property int $submicoes_id
 * @property bool $ativo
 *
 * @property \App\Model\Entity\Modalidade $modalidade
 * @property \App\Model\Entity\Submico $submico
 */
class Modalidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'modalidades_id' => false
    ];
}
