<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evento Entity
 *
 * @property int $eventos_id
 * @property string $titulo
 * @property string $apresentacao
 * @property string $edital
 * @property string $template
 * @property \Cake\I18n\Time $data_ini
 * @property \Cake\I18n\Time $data_fim
 * @property string $foto_evento
 * @property string $programacao
 * @property string $inf_horario
 * @property bool $trabalho
 * @property bool $ativo
 * @property string $logo
 *
 * @property \App\Model\Entity\Evento $evento
 * @property \App\Model\Entity\SubEvento[] $sub_eventos
 */
class Evento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'eventos_id' => false
    ];
}
