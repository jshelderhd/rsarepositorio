<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EventosSubEvento Entity
 *
 * @property int $eventos_sub_eventos_id
 * @property int $eventos_id
 * @property int $sub_eventos_id
 * @property float $valor_ini
 * @property float $valor_fim
 * @property \Cake\I18n\Time $data_alteraccao
 * @property string $nome
 * @property string $descricao
 * @property \Cake\I18n\Time $data_sub_evento
 * @property bool $ativo
 *
 * @property \App\Model\Entity\EventosSubEvento $eventos_sub_evento
 * @property \App\Model\Entity\Evento $evento
 * @property \App\Model\Entity\SubEvento $sub_evento
 */
class EventosSubEvento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'eventos_sub_eventos_id' => false
    ];
}
