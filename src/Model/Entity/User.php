<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $usuarios_id
 * @property string $nome_completo
 * @property string $sexo
 * @property string $cpf
 * @property string $username
 * @property string $telefone
 * @property string $password
 * @property \Cake\I18n\Time $data_criacao
 * @property int $instituicoes_id
 * @property int $tipo_acesso
 * @property bool $ativo
 *
 * @property \App\Model\Entity\Usuario $usuario
 * @property \App\Model\Entity\Instituico $instituico
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'usuarios_id' => false
    ];
   protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }


    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
