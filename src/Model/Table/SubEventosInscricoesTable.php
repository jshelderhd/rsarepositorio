<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SubEventosInscricoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $EventosSubEventos
 * @property \Cake\ORM\Association\BelongsTo $Inscricoes
 *
 * @method \App\Model\Entity\SubEventosInscrico get($primaryKey, $options = [])
 * @method \App\Model\Entity\SubEventosInscrico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SubEventosInscrico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SubEventosInscrico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SubEventosInscrico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SubEventosInscrico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SubEventosInscrico findOrCreate($search, callable $callback = null, $options = [])
 */
class SubEventosInscricoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sub_eventos_inscricoes');
        $this->setDisplayField('evento_sub_eventos_id');
        $this->setPrimaryKey(['evento_sub_eventos_id', 'inscricoes_id']);

        $this->belongsTo('EventosSubEventos', [
            'foreignKey' => 'evento_sub_eventos_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Inscricoes', [
            'foreignKey' => 'inscricoes_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['evento_sub_eventos_id'], 'EventosSubEventos'));
        $rules->add($rules->existsIn(['inscricoes_id'], 'Inscricoes'));

        return $rules;
    }
}
