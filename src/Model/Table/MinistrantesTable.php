<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ministrantes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Ministrantes
 * @property \Cake\ORM\Association\BelongsTo $EventosSubEventos
 *
 * @method \App\Model\Entity\Ministrante get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ministrante newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ministrante[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ministrante|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ministrante patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ministrante[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ministrante findOrCreate($search, callable $callback = null, $options = [])
 */
class MinistrantesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ministrantes');
        $this->setDisplayField('ministrante_id');
        $this->setPrimaryKey('ministrante_id');

        $this->belongsTo('Ministrantes', [
            'foreignKey' => 'ministrante_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('EventosSubEventos', [
            'foreignKey' => 'Eventos_sub_eventos_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('nome_ministrante');

        $validator
            ->allowEmpty('foto_ministrante');

        $validator
            ->allowEmpty('sobre');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ministrante_id'], 'Ministrantes'));
        $rules->add($rules->existsIn(['Eventos_sub_eventos_id'], 'EventosSubEventos'));

        return $rules;
    }
}
