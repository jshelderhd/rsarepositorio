<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EventosSubEventos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $EventosSubEventos
 * @property \Cake\ORM\Association\BelongsTo $Eventos
 * @property \Cake\ORM\Association\BelongsTo $SubEventos
 *
 * @method \App\Model\Entity\EventosSubEvento get($primaryKey, $options = [])
 * @method \App\Model\Entity\EventosSubEvento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EventosSubEvento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EventosSubEvento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EventosSubEvento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EventosSubEvento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EventosSubEvento findOrCreate($search, callable $callback = null, $options = [])
 */
class EventosSubEventosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('eventos_sub_eventos');
        $this->setDisplayField('eventos_sub_eventos_id');
        $this->setPrimaryKey('eventos_sub_eventos_id');

        $this->belongsTo('EventosSubEventos', [
            'foreignKey' => 'eventos_sub_eventos_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Eventos', [
            'foreignKey' => 'eventos_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SubEventos', [
            'foreignKey' => 'sub_eventos_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->numeric('valor_ini')
            ->allowEmpty('valor_ini');

        $validator
            ->numeric('valor_fim')
            ->allowEmpty('valor_fim');

        $validator
            ->date('data_alteraccao')
            ->allowEmpty('data_alteraccao');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->dateTime('data_sub_evento')
            ->allowEmpty('data_sub_evento');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['eventos_sub_eventos_id'], 'EventosSubEventos'));
        $rules->add($rules->existsIn(['eventos_id'], 'Eventos'));
        $rules->add($rules->existsIn(['sub_eventos_id'], 'SubEventos'));

        return $rules;
    }
}
