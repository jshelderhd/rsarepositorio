<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Eventos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Eventos
 * @property \Cake\ORM\Association\BelongsToMany $SubEventos
 *
 * @method \App\Model\Entity\Evento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Evento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Evento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Evento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Evento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Evento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Evento findOrCreate($search, callable $callback = null, $options = [])
 */
class EventosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('eventos');
        $this->setDisplayField('eventos_id');
        $this->setPrimaryKey('eventos_id');
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'foto_evento' => [
                'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    
                    'size' => 'photo_size', // defaults to `size`
                    'type' => 'photo_type', // defaults to `type`
                ],
            ],
            'logo' => [
                'fields' => [

                    'size' => '128x128', // defaults to `size`
                    'type' => 'photo_type', // defaults to `type`
                ],
            ],
            'template' => [
                'fields' => [

                ],
            ],
            'edital' => [
                'fields' => [

                ],
            ],
        ]);


        $this->belongsTo('Eventos', [
            'foreignKey' => 'eventos_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('SubEventos', [
            'foreignKey' => 'evento_id',
            'targetForeignKey' => 'sub_evento_id',
            'joinTable' => 'eventos_sub_eventos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('titulo');

        $validator
            ->allowEmpty('apresentacao');

        $validator
            ->allowEmpty('edital');

        $validator
            ->allowEmpty('template');

        $validator
            ->allowEmpty('data_ini');

        $validator
            ->allowEmpty('data_fim');

        $validator
            ->allowEmpty('foto_evento');

        $validator
            ->allowEmpty('programacao');

        $validator
            ->allowEmpty('inf_horario');

        $validator
            ->boolean('trabalho')
            ->allowEmpty('trabalho');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        $validator
            ->requirePresence('logo', 'create')
            ->notEmpty('logo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['eventos_id'], 'Eventos'));

        return $rules;
    }
}
