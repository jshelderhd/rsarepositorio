<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Modalidades Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Modalidades
 * @property \Cake\ORM\Association\BelongsTo $Submicoes
 *
 * @method \App\Model\Entity\Modalidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\Modalidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Modalidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Modalidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Modalidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Modalidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Modalidade findOrCreate($search, callable $callback = null, $options = [])
 */
class ModalidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('modalidades');
        $this->setDisplayField('modalidades_id');
        $this->setPrimaryKey('modalidades_id');

        $this->belongsTo('Modalidades', [
            'foreignKey' => 'modalidades_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Submicoes', [
            'foreignKey' => 'submicoes_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('nome');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['modalidades_id'], 'Modalidades'));
        $rules->add($rules->existsIn(['submicoes_id'], 'Submicoes'));

        return $rules;
    }
}
