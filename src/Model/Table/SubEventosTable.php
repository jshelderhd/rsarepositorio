<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SubEventos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SubEventos
 * @property \Cake\ORM\Association\BelongsToMany $Eventos
 * @property \Cake\ORM\Association\BelongsToMany $Inscricoes
 *
 * @method \App\Model\Entity\SubEvento get($primaryKey, $options = [])
 * @method \App\Model\Entity\SubEvento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SubEvento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SubEvento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SubEvento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SubEvento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SubEvento findOrCreate($search, callable $callback = null, $options = [])
 */
class SubEventosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sub_eventos');
        $this->setDisplayField('sub_eventos_id');
        $this->setPrimaryKey('sub_eventos_id');

        $this->belongsTo('SubEventos', [
            'foreignKey' => 'sub_eventos_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Eventos', [
            'foreignKey' => 'sub_evento_id',
            'targetForeignKey' => 'evento_id',
            'joinTable' => 'eventos_sub_eventos'
        ]);
        $this->belongsToMany('Inscricoes', [
            'foreignKey' => 'sub_evento_id',
            'targetForeignKey' => 'inscrico_id',
            'joinTable' => 'sub_eventos_inscricoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('nome');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sub_eventos_id'], 'SubEventos'));

        return $rules;
    }
}
