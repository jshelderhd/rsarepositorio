<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Submicoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Submicoes
 * @property \Cake\ORM\Association\BelongsTo $Inscricoes
 * @property \Cake\ORM\Association\BelongsTo $Cursos
 *
 * @method \App\Model\Entity\Submico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Submico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Submico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Submico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Submico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Submico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Submico findOrCreate($search, callable $callback = null, $options = [])
 */
class SubmicoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('submicoes');
        $this->setDisplayField('submicoes_id');
        $this->setPrimaryKey('submicoes_id');

        $this->belongsTo('Submicoes', [
            'foreignKey' => 'submicoes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Inscricoes', [
            'foreignKey' => 'inscricoes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cursos', [
            'foreignKey' => 'cursos_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('titulo');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['submicoes_id'], 'Submicoes'));
        $rules->add($rules->existsIn(['inscricoes_id'], 'Inscricoes'));
        $rules->add($rules->existsIn(['cursos_id'], 'Cursos'));

        return $rules;
    }
}
