<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Inscricoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Inscricoes
 * @property \Cake\ORM\Association\BelongsTo $Eventos
 * @property \Cake\ORM\Association\BelongsTo $Usuarios
 * @property \Cake\ORM\Association\BelongsTo $Inscricoes
 * @property \Cake\ORM\Association\BelongsToMany $SubEventos
 *
 * @method \App\Model\Entity\Inscrico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Inscrico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Inscrico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Inscrico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscrico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Inscrico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Inscrico findOrCreate($search, callable $callback = null, $options = [])
 */
class InscricoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('inscricoes');
        $this->setDisplayField('inscricoes_id');
        $this->setPrimaryKey('inscricoes_id');

        $this->belongsTo('Inscricoes', [
            'foreignKey' => 'inscricoes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Eventos', [
            'foreignKey' => 'eventos_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuarios', [
            'foreignKey' => 'usuarios_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Inscricoes', [
            'foreignKey' => 'autor_inscricoes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('SubEventos', [
            'foreignKey' => 'inscrico_id',
            'targetForeignKey' => 'sub_evento_id',
            'joinTable' => 'sub_eventos_inscricoes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->boolean('tipo_insc')
            ->allowEmpty('tipo_insc');

        $validator
            ->dateTime('data_inscricao')
            ->allowEmpty('data_inscricao');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['inscricoes_id'], 'Inscricoes'));
        $rules->add($rules->existsIn(['eventos_id'], 'Eventos'));
        $rules->add($rules->existsIn(['usuarios_id'], 'Usuarios'));
        $rules->add($rules->existsIn(['autor_inscricoes_id'], 'Inscricoes'));

        return $rules;
    }
}
