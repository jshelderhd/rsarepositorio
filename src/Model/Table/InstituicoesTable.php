<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Instituicoes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Instituicoes
 * @property \Cake\ORM\Association\BelongsTo $Cursos
 *
 * @method \App\Model\Entity\Instituico get($primaryKey, $options = [])
 * @method \App\Model\Entity\Instituico newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Instituico[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Instituico|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Instituico patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Instituico[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Instituico findOrCreate($search, callable $callback = null, $options = [])
 */
class InstituicoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('instituicoes');
        $this->setDisplayField('instituicoes_id');
        $this->setPrimaryKey('instituicoes_id');

        $this->belongsTo('Instituicoes', [
            'foreignKey' => 'instituicoes_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cursos', [
            'foreignKey' => 'cursos_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('nome');

        $validator
            ->boolean('ativo')
            ->allowEmpty('ativo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['instituicoes_id'], 'Instituicoes'));
        $rules->add($rules->existsIn(['cursos_id'], 'Cursos'));

        return $rules;
    }
}
