<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EventosSubEventos Controller
 *
 * @property \App\Model\Table\EventosSubEventosTable $EventosSubEventos
 */
class EventosSubEventosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['EventosSubEventos', 'Eventos', 'SubEventos']
        ];
        $eventosSubEventos = $this->paginate($this->EventosSubEventos);

        $this->set(compact('eventosSubEventos'));
        $this->set('_serialize', ['eventosSubEventos']);
    }

    /**
     * View method
     *
     * @param string|null $id Eventos Sub Evento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $eventosSubEvento = $this->EventosSubEventos->get($id, [
            'contain' => ['EventosSubEventos', 'Eventos', 'SubEventos']
        ]);

        $this->set('eventosSubEvento', $eventosSubEvento);
        $this->set('_serialize', ['eventosSubEvento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $eventosSubEvento = $this->EventosSubEventos->newEntity();
        if ($this->request->is('post')) {
            $eventosSubEvento = $this->EventosSubEventos->patchEntity($eventosSubEvento, $this->request->getData());
            if ($this->EventosSubEventos->save($eventosSubEvento)) {
                $this->Flash->success(__('The eventos sub evento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The eventos sub evento could not be saved. Please, try again.'));
        }
        $eventosSubEventos = $this->EventosSubEventos->EventosSubEventos->find('list', ['limit' => 200]);
        $eventos = $this->EventosSubEventos->Eventos->find('list', ['limit' => 200]);
        $subEventos = $this->EventosSubEventos->SubEventos->find('list', ['limit' => 200]);
        $this->set(compact('eventosSubEvento', 'eventosSubEventos', 'eventos', 'subEventos'));
        $this->set('_serialize', ['eventosSubEvento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Eventos Sub Evento id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $eventosSubEvento = $this->EventosSubEventos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $eventosSubEvento = $this->EventosSubEventos->patchEntity($eventosSubEvento, $this->request->getData());
            if ($this->EventosSubEventos->save($eventosSubEvento)) {
                $this->Flash->success(__('The eventos sub evento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The eventos sub evento could not be saved. Please, try again.'));
        }
        $eventosSubEventos = $this->EventosSubEventos->EventosSubEventos->find('list', ['limit' => 200]);
        $eventos = $this->EventosSubEventos->Eventos->find('list', ['limit' => 200]);
        $subEventos = $this->EventosSubEventos->SubEventos->find('list', ['limit' => 200]);
        $this->set(compact('eventosSubEvento', 'eventosSubEventos', 'eventos', 'subEventos'));
        $this->set('_serialize', ['eventosSubEvento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Eventos Sub Evento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $eventosSubEvento = $this->EventosSubEventos->get($id);
        if ($this->EventosSubEventos->delete($eventosSubEvento)) {
            $this->Flash->success(__('The eventos sub evento has been deleted.'));
        } else {
            $this->Flash->error(__('The eventos sub evento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
