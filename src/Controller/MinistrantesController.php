<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ministrantes Controller
 *
 * @property \App\Model\Table\MinistrantesTable $Ministrantes
 */
class MinistrantesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ministrantes', 'EventosSubEventos']
        ];
        $ministrantes = $this->paginate($this->Ministrantes);

        $this->set(compact('ministrantes'));
        $this->set('_serialize', ['ministrantes']);
    }

    /**
     * View method
     *
     * @param string|null $id Ministrante id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ministrante = $this->Ministrantes->get($id, [
            'contain' => ['Ministrantes', 'EventosSubEventos']
        ]);

        $this->set('ministrante', $ministrante);
        $this->set('_serialize', ['ministrante']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ministrante = $this->Ministrantes->newEntity();
        if ($this->request->is('post')) {
            $ministrante = $this->Ministrantes->patchEntity($ministrante, $this->request->getData());
            if ($this->Ministrantes->save($ministrante)) {
                $this->Flash->success(__('The ministrante has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ministrante could not be saved. Please, try again.'));
        }
        $ministrantes = $this->Ministrantes->Ministrantes->find('list', ['limit' => 200]);
        $eventosSubEventos = $this->Ministrantes->EventosSubEventos->find('list', ['limit' => 200]);
        $this->set(compact('ministrante', 'ministrantes', 'eventosSubEventos'));
        $this->set('_serialize', ['ministrante']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ministrante id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ministrante = $this->Ministrantes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ministrante = $this->Ministrantes->patchEntity($ministrante, $this->request->getData());
            if ($this->Ministrantes->save($ministrante)) {
                $this->Flash->success(__('The ministrante has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ministrante could not be saved. Please, try again.'));
        }
        $ministrantes = $this->Ministrantes->Ministrantes->find('list', ['limit' => 200]);
        $eventosSubEventos = $this->Ministrantes->EventosSubEventos->find('list', ['limit' => 200]);
        $this->set(compact('ministrante', 'ministrantes', 'eventosSubEventos'));
        $this->set('_serialize', ['ministrante']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ministrante id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ministrante = $this->Ministrantes->get($id);
        if ($this->Ministrantes->delete($ministrante)) {
            $this->Flash->success(__('The ministrante has been deleted.'));
        } else {
            $this->Flash->error(__('The ministrante could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
