<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Inscricoes Controller
 *
 * @property \App\Model\Table\InscricoesTable $Inscricoes
 */
class InscricoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Inscricoes', 'Eventos', 'Usuarios']
        ];
        $inscricoes = $this->paginate($this->Inscricoes);

        $this->set(compact('inscricoes'));
        $this->set('_serialize', ['inscricoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Inscrico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inscrico = $this->Inscricoes->get($id, [
            'contain' => ['Inscricoes', 'Eventos', 'Usuarios', 'SubEventos']
        ]);

        $this->set('inscrico', $inscrico);
        $this->set('_serialize', ['inscrico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $inscrico = $this->Inscricoes->newEntity();
        if ($this->request->is('post')) {
            $inscrico = $this->Inscricoes->patchEntity($inscrico, $this->request->getData());
            if ($this->Inscricoes->save($inscrico)) {
                $this->Flash->success(__('The inscrico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The inscrico could not be saved. Please, try again.'));
        }
        $inscricoes = $this->Inscricoes->Inscricoes->find('list', ['limit' => 200]);
        $eventos = $this->Inscricoes->Eventos->find('list', ['limit' => 200]);
        $usuarios = $this->Inscricoes->Usuarios->find('list', ['limit' => 200]);
        $subEventos = $this->Inscricoes->SubEventos->find('list', ['limit' => 200]);
        $this->set(compact('inscrico', 'inscricoes', 'eventos', 'usuarios', 'subEventos'));
        $this->set('_serialize', ['inscrico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Inscrico id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $inscrico = $this->Inscricoes->get($id, [
            'contain' => ['SubEventos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $inscrico = $this->Inscricoes->patchEntity($inscrico, $this->request->getData());
            if ($this->Inscricoes->save($inscrico)) {
                $this->Flash->success(__('The inscrico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The inscrico could not be saved. Please, try again.'));
        }
        $inscricoes = $this->Inscricoes->Inscricoes->find('list', ['limit' => 200]);
        $eventos = $this->Inscricoes->Eventos->find('list', ['limit' => 200]);
        $usuarios = $this->Inscricoes->Usuarios->find('list', ['limit' => 200]);
        $subEventos = $this->Inscricoes->SubEventos->find('list', ['limit' => 200]);
        $this->set(compact('inscrico', 'inscricoes', 'eventos', 'usuarios', 'subEventos'));
        $this->set('_serialize', ['inscrico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Inscrico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $inscrico = $this->Inscricoes->get($id);
        if ($this->Inscricoes->delete($inscrico)) {
            $this->Flash->success(__('The inscrico has been deleted.'));
        } else {
            $this->Flash->error(__('The inscrico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
