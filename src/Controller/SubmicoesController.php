<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Submicoes Controller
 *
 * @property \App\Model\Table\SubmicoesTable $Submicoes
 */
class SubmicoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Submicoes', 'Inscricoes', 'Cursos']
        ];
        $submicoes = $this->paginate($this->Submicoes);

        $this->set(compact('submicoes'));
        $this->set('_serialize', ['submicoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Submico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $submico = $this->Submicoes->get($id, [
            'contain' => ['Submicoes', 'Inscricoes', 'Cursos']
        ]);

        $this->set('submico', $submico);
        $this->set('_serialize', ['submico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $submico = $this->Submicoes->newEntity();
        if ($this->request->is('post')) {
            $submico = $this->Submicoes->patchEntity($submico, $this->request->getData());
            if ($this->Submicoes->save($submico)) {
                $this->Flash->success(__('The submico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submico could not be saved. Please, try again.'));
        }
        $submicoes = $this->Submicoes->Submicoes->find('list', ['limit' => 200]);
        $inscricoes = $this->Submicoes->Inscricoes->find('list', ['limit' => 200]);
        $cursos = $this->Submicoes->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('submico', 'submicoes', 'inscricoes', 'cursos'));
        $this->set('_serialize', ['submico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Submico id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $submico = $this->Submicoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $submico = $this->Submicoes->patchEntity($submico, $this->request->getData());
            if ($this->Submicoes->save($submico)) {
                $this->Flash->success(__('The submico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submico could not be saved. Please, try again.'));
        }
        $submicoes = $this->Submicoes->Submicoes->find('list', ['limit' => 200]);
        $inscricoes = $this->Submicoes->Inscricoes->find('list', ['limit' => 200]);
        $cursos = $this->Submicoes->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('submico', 'submicoes', 'inscricoes', 'cursos'));
        $this->set('_serialize', ['submico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Submico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $submico = $this->Submicoes->get($id);
        if ($this->Submicoes->delete($submico)) {
            $this->Flash->success(__('The submico has been deleted.'));
        } else {
            $this->Flash->error(__('The submico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
