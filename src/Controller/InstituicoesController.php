<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Instituicoes Controller
 *
 * @property \App\Model\Table\InstituicoesTable $Instituicoes
 */
class InstituicoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [ 'Cursos']
        ];
        $instituicoes = $this->paginate($this->Instituicoes);

        $this->set(compact('instituicoes'));
        $this->set('_serialize', ['instituicoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Instituico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $instituico = $this->Instituicoes->get($id, [
            'contain' => ['Instituicoes', 'Cursos']
        ]);

        $this->set('instituico', $instituico);
        $this->set('_serialize', ['instituico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $instituico = $this->Instituicoes->newEntity();
        if ($this->request->is('post')) {
            $instituico = $this->Instituicoes->patchEntity($instituico, $this->request->getData());
            if ($this->Instituicoes->save($instituico)) {
                $this->Flash->success(__('The instituico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The instituico could not be saved. Please, try again.'));
        }
        $instituicoes = $this->Instituicoes->Instituicoes->find('list', ['limit' => 200]);
        $cursos = $this->Instituicoes->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('instituico', 'instituicoes', 'cursos'));
        $this->set('_serialize', ['instituico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Instituico id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $instituico = $this->Instituicoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $instituico = $this->Instituicoes->patchEntity($instituico, $this->request->getData());
            if ($this->Instituicoes->save($instituico)) {
                $this->Flash->success(__('The instituico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The instituico could not be saved. Please, try again.'));
        }
        $instituicoes = $this->Instituicoes->Instituicoes->find('list', ['limit' => 200]);
        $cursos = $this->Instituicoes->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('instituico', 'instituicoes', 'cursos'));
        $this->set('_serialize', ['instituico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Instituico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $instituico = $this->Instituicoes->get($id);
        if ($this->Instituicoes->delete($instituico)) {
            $this->Flash->success(__('The instituico has been deleted.'));
        } else {
            $this->Flash->error(__('The instituico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
