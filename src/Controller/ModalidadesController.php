<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Modalidades Controller
 *
 * @property \App\Model\Table\ModalidadesTable $Modalidades
 */
class ModalidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Modalidades', 'Submicoes']
        ];
        $modalidades = $this->paginate($this->Modalidades);

        $this->set(compact('modalidades'));
        $this->set('_serialize', ['modalidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Modalidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $modalidade = $this->Modalidades->get($id, [
            'contain' => ['Modalidades', 'Submicoes']
        ]);

        $this->set('modalidade', $modalidade);
        $this->set('_serialize', ['modalidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $modalidade = $this->Modalidades->newEntity();
        if ($this->request->is('post')) {
            $modalidade = $this->Modalidades->patchEntity($modalidade, $this->request->getData());
            if ($this->Modalidades->save($modalidade)) {
                $this->Flash->success(__('The modalidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The modalidade could not be saved. Please, try again.'));
        }
        $modalidades = $this->Modalidades->Modalidades->find('list', ['limit' => 200]);
        $submicoes = $this->Modalidades->Submicoes->find('list', ['limit' => 200]);
        $this->set(compact('modalidade', 'modalidades', 'submicoes'));
        $this->set('_serialize', ['modalidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Modalidade id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $modalidade = $this->Modalidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modalidade = $this->Modalidades->patchEntity($modalidade, $this->request->getData());
            if ($this->Modalidades->save($modalidade)) {
                $this->Flash->success(__('The modalidade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The modalidade could not be saved. Please, try again.'));
        }
        $modalidades = $this->Modalidades->Modalidades->find('list', ['limit' => 200]);
        $submicoes = $this->Modalidades->Submicoes->find('list', ['limit' => 200]);
        $this->set(compact('modalidade', 'modalidades', 'submicoes'));
        $this->set('_serialize', ['modalidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Modalidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $modalidade = $this->Modalidades->get($id);
        if ($this->Modalidades->delete($modalidade)) {
            $this->Flash->success(__('The modalidade has been deleted.'));
        } else {
            $this->Flash->error(__('The modalidade could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
