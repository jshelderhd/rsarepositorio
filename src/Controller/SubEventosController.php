<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SubEventos Controller
 *
 * @property \App\Model\Table\SubEventosTable $SubEventos
 */
class SubEventosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SubEventos']
        ];
        $subEventos = $this->paginate($this->SubEventos);

        $this->set(compact('subEventos'));
        $this->set('_serialize', ['subEventos']);
    }

    /**
     * View method
     *
     * @param string|null $id Sub Evento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subEvento = $this->SubEventos->get($id, [
            'contain' => ['SubEventos', 'Eventos', 'Inscricoes']
        ]);

        $this->set('subEvento', $subEvento);
        $this->set('_serialize', ['subEvento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subEvento = $this->SubEventos->newEntity();
        if ($this->request->is('post')) {
            $subEvento = $this->SubEventos->patchEntity($subEvento, $this->request->getData());
            if ($this->SubEventos->save($subEvento)) {
                $this->Flash->success(__('The sub evento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sub evento could not be saved. Please, try again.'));
        }
        $subEventos = $this->SubEventos->SubEventos->find('list', ['limit' => 200]);
        $eventos = $this->SubEventos->Eventos->find('list', ['limit' => 200]);
        $inscricoes = $this->SubEventos->Inscricoes->find('list', ['limit' => 200]);
        $this->set(compact('subEvento', 'subEventos', 'eventos', 'inscricoes'));
        $this->set('_serialize', ['subEvento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sub Evento id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subEvento = $this->SubEventos->get($id, [
            'contain' => ['Eventos', 'Inscricoes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subEvento = $this->SubEventos->patchEntity($subEvento, $this->request->getData());
            if ($this->SubEventos->save($subEvento)) {
                $this->Flash->success(__('The sub evento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sub evento could not be saved. Please, try again.'));
        }
        $subEventos = $this->SubEventos->SubEventos->find('list', ['limit' => 200]);
        $eventos = $this->SubEventos->Eventos->find('list', ['limit' => 200]);
        $inscricoes = $this->SubEventos->Inscricoes->find('list', ['limit' => 200]);
        $this->set(compact('subEvento', 'subEventos', 'eventos', 'inscricoes'));
        $this->set('_serialize', ['subEvento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sub Evento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subEvento = $this->SubEventos->get($id);
        if ($this->SubEventos->delete($subEvento)) {
            $this->Flash->success(__('The sub evento has been deleted.'));
        } else {
            $this->Flash->error(__('The sub evento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
