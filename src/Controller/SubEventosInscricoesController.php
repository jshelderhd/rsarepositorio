<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SubEventosInscricoes Controller
 *
 * @property \App\Model\Table\SubEventosInscricoesTable $SubEventosInscricoes
 */
class SubEventosInscricoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['EventosSubEventos', 'Inscricoes', 'SubEventos']
        ];
        $subEventosInscricoes = $this->paginate($this->SubEventosInscricoes);

        $this->set(compact('subEventosInscricoes'));
        $this->set('_serialize', ['subEventosInscricoes']);
    }

    /**
     * View method
     *
     * @param string|null $id Sub Eventos Inscrico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subEventosInscrico = $this->SubEventosInscricoes->get($id, [
            'contain' => ['EventosSubEventos', 'Inscricoes', 'SubEventos']
        ]);

        $this->set('subEventosInscrico', $subEventosInscrico);
        $this->set('_serialize', ['subEventosInscrico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subEventosInscrico = $this->SubEventosInscricoes->newEntity();
        if ($this->request->is('post')) {
            $subEventosInscrico = $this->SubEventosInscricoes->patchEntity($subEventosInscrico, $this->request->getData());
            if ($this->SubEventosInscricoes->save($subEventosInscrico)) {
                $this->Flash->success(__('The sub eventos inscrico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sub eventos inscrico could not be saved. Please, try again.'));
        }
        $eventosSubEventos = $this->SubEventosInscricoes->EventosSubEventos->find('list', ['limit' => 200]);
        $inscricoes = $this->SubEventosInscricoes->Inscricoes->find('list', ['limit' => 200]);
        $subEventos = $this->SubEventosInscricoes->SubEventos->find('list', ['limit' => 200]);
        $this->set(compact('subEventosInscrico', 'eventosSubEventos', 'inscricoes', 'subEventos'));
        $this->set('_serialize', ['subEventosInscrico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sub Eventos Inscrico id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subEventosInscrico = $this->SubEventosInscricoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subEventosInscrico = $this->SubEventosInscricoes->patchEntity($subEventosInscrico, $this->request->getData());
            if ($this->SubEventosInscricoes->save($subEventosInscrico)) {
                $this->Flash->success(__('The sub eventos inscrico has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sub eventos inscrico could not be saved. Please, try again.'));
        }
        $eventosSubEventos = $this->SubEventosInscricoes->EventosSubEventos->find('list', ['limit' => 200]);
        $inscricoes = $this->SubEventosInscricoes->Inscricoes->find('list', ['limit' => 200]);
        $subEventos = $this->SubEventosInscricoes->SubEventos->find('list', ['limit' => 200]);
        $this->set(compact('subEventosInscrico', 'eventosSubEventos', 'inscricoes', 'subEventos'));
        $this->set('_serialize', ['subEventosInscrico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sub Eventos Inscrico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subEventosInscrico = $this->SubEventosInscricoes->get($id);
        if ($this->SubEventosInscricoes->delete($subEventosInscrico)) {
            $this->Flash->success(__('The sub eventos inscrico has been deleted.'));
        } else {
            $this->Flash->error(__('The sub eventos inscrico could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
