<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Eventos Sub Evento']), ['action' => 'edit', $eventosSubEvento->eventos_sub_eventos_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Eventos Sub Evento']), ['action' => 'delete', $eventosSubEvento->eventos_sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $eventosSubEvento->eventos_sub_eventos_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos Sub Eventos']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos Sub Eventos']), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="eventosSubEventos view col-lg-10 col-md-9">
    <h3><?= h($eventosSubEvento->eventos_sub_eventos_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Eventos Sub Evento</th>
            <td><?= $eventosSubEvento->has('eventos_sub_evento') ? $this->Html->link($eventosSubEvento->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $eventosSubEvento->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Evento</th>
            <td><?= $eventosSubEvento->has('evento') ? $this->Html->link($eventosSubEvento->evento->eventos_id, ['controller' => 'Eventos', 'action' => 'view', $eventosSubEvento->evento->eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Sub Evento</th>
            <td><?= $eventosSubEvento->has('sub_evento') ? $this->Html->link($eventosSubEvento->sub_evento->sub_eventos_id, ['controller' => 'SubEventos', 'action' => 'view', $eventosSubEvento->sub_evento->sub_eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Nome</th>
            <td><?= h($eventosSubEvento->nome) ?></td>
        </tr>
        <tr>
            <th>'Valor Ini</th>
            <td><?= $this->Number->format($eventosSubEvento->valor_ini) ?></td>
        </tr>
        <tr>
            <th>'Valor Fim</th>
            <td><?= $this->Number->format($eventosSubEvento->valor_fim) ?></td>
        </tr>
        <tr>
            <th>Data Alteraccao</th>
            <td><?= h($eventosSubEvento->data_alteraccao) ?></tr>
        </tr>
        <tr>
            <th>Data Sub Evento</th>
            <td><?= h($eventosSubEvento->data_sub_evento) ?></tr>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $eventosSubEvento->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="row">
        <h4>Descricao</h4>
        <?= $this->Text->autoParagraph(h($eventosSubEvento->descricao)); ?>
    </div>
</div>
