<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="eventosSubEventos index col-md-10 columns content">
    <h3>Eventos Sub Eventos</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('eventos_sub_eventos_id') ?></th>
                <th><?= $this->Paginator->sort('eventos_id') ?></th>
                <th><?= $this->Paginator->sort('sub_eventos_id') ?></th>
                <th><?= $this->Paginator->sort('valor_ini') ?></th>
                <th><?= $this->Paginator->sort('valor_fim') ?></th>
                <th><?= $this->Paginator->sort('data_alteraccao') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($eventosSubEventos as $eventosSubEvento): ?>
            <tr>
                <td><?= $eventosSubEvento->has('eventos_sub_evento') ? $this->Html->link($eventosSubEvento->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $eventosSubEvento->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
                <td><?= $eventosSubEvento->has('evento') ? $this->Html->link($eventosSubEvento->evento->eventos_id, ['controller' => 'Eventos', 'action' => 'view', $eventosSubEvento->evento->eventos_id]) : '' ?></td>
                <td><?= $eventosSubEvento->has('sub_evento') ? $this->Html->link($eventosSubEvento->sub_evento->sub_eventos_id, ['controller' => 'SubEventos', 'action' => 'view', $eventosSubEvento->sub_evento->sub_eventos_id]) : '' ?></td>
                <td><?= $this->Number->format($eventosSubEvento->valor_ini) ?></td>
                <td><?= $this->Number->format($eventosSubEvento->valor_fim) ?></td>
                <td><?= h($eventosSubEvento->data_alteraccao) ?></td>
                <td><?= h($eventosSubEvento->nome) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $eventosSubEvento->eventos_sub_eventos_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $eventosSubEvento->eventos_sub_eventos_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $eventosSubEvento->eventos_sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $eventosSubEvento->eventos_sub_eventos_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>