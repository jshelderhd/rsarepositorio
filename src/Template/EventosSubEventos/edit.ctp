<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $eventosSubEvento->eventos_sub_eventos_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $eventosSubEvento->eventos_sub_eventos_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos Sub Eventos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos Sub Eventos'), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Eventos Sub Evento'), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos'), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Evento'), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos'), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Sub Evento'), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="eventosSubEventos form col-md-10 columns content">
    <?= $this->Form->create($eventosSubEvento) ?>
    <fieldset>
        <legend><?= 'Edit Eventos Sub Evento' ?></legend>
        <?php
            echo $this->Form->input('eventos_id', ['options' => $eventos]);
            echo $this->Form->input('sub_eventos_id', ['options' => $subEventos]);
            echo $this->Form->input('valor_ini');
            echo $this->Form->input('valor_fim');
            echo $this->Form->input('data_alteraccao', ['empty' => true, 'default' => '']);
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
            echo $this->Form->input('data_sub_evento');
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
