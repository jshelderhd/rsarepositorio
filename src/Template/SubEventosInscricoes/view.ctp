<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Sub Eventos Inscrico']), ['action' => 'edit', $subEventosInscrico->evento_sub_eventos_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Sub Eventos Inscrico']), ['action' => 'delete', $subEventosInscrico->evento_sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEventosInscrico->evento_sub_eventos_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos Inscricoes']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Eventos Inscrico']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos Sub Eventos']), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subEventosInscricoes view col-lg-10 col-md-9">
    <h3><?= h($subEventosInscrico->evento_sub_eventos_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Eventos Sub Evento</th>
            <td><?= $subEventosInscrico->has('eventos_sub_evento') ? $this->Html->link($subEventosInscrico->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $subEventosInscrico->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Inscrico</th>
            <td><?= $subEventosInscrico->has('inscrico') ? $this->Html->link($subEventosInscrico->inscrico->inscricoes_id, ['controller' => 'Inscricoes', 'action' => 'view', $subEventosInscrico->inscrico->inscricoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $subEventosInscrico->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
