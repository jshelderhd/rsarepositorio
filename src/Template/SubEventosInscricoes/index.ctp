<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Eventos Inscrico']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos Sub Eventos']), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subEventosInscricoes index col-md-10 columns content">
    <h3>Sub Eventos Inscricoes</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('evento_sub_eventos_id') ?></th>
                <th><?= $this->Paginator->sort('inscricoes_id') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subEventosInscricoes as $subEventosInscrico): ?>
            <tr>
                <td><?= $subEventosInscrico->has('eventos_sub_evento') ? $this->Html->link($subEventosInscrico->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $subEventosInscrico->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
                <td><?= $subEventosInscrico->has('inscrico') ? $this->Html->link($subEventosInscrico->inscrico->inscricoes_id, ['controller' => 'Inscricoes', 'action' => 'view', $subEventosInscrico->inscrico->inscricoes_id]) : '' ?></td>
                <td><?= h($subEventosInscrico->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $subEventosInscrico->evento_sub_eventos_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $subEventosInscrico->evento_sub_eventos_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $subEventosInscrico->evento_sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEventosInscrico->evento_sub_eventos_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>