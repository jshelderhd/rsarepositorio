<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos Inscricoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos Sub Eventos'), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Eventos Sub Evento'), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Inscricoes'), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Inscrico'), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos'), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Sub Evento'), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subEventosInscricoes form col-md-10 columns content">
    <?= $this->Form->create($subEventosInscrico) ?>
    <fieldset>
        <legend><?= 'Add Sub Eventos Inscrico' ?></legend>
        <?php
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
