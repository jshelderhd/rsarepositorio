<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Curso']), ['action' => 'edit', $curso->cursos_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Curso']), ['action' => 'delete', $curso->cursos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $curso->cursos_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Cursos']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Curso']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Cursos']), ['controller' => 'Cursos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Curso']), ['controller' => 'Cursos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cursos view col-lg-10 col-md-9">
    <h3><?= h($curso->cursos_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Curso</th>
            <td><?= $curso->has('curso') ? $this->Html->link($curso->curso->cursos_id, ['controller' => 'Cursos', 'action' => 'view', $curso->curso->cursos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Nome</th>
            <td><?= h($curso->nome) ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $curso->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
