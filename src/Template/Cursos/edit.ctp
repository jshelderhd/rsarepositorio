<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $curso->cursos_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $curso->cursos_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Cursos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Cursos'), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Curso'), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cursos form col-md-10 columns content">
    <?= $this->Form->create($curso) ?>
    <fieldset>
        <legend><?= 'Edit Curso' ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
