
<div class="row ">

    <nav class="navbar navbar-default" style="padding-left: 10px; padding-right: 10px; background:#64a7ce; color: white">
   <div class="container-fluid">
     <div class="navbar-header">
         <a style="color: white;"class="brand navbar-brand center-block " href="#">Cursos</a>
     </div>
       <ul class="nav navbar-nav navbar-right" style="padding-right: 10px;">
       
         <li style="font-size: 20px;"><?=$this->Html->link("<i class='fa fa-plus-circle' title='Adicionar Curso'></i>", ['action'=>'add'], ['escape' => false]) ?></li>
     </ul>
   </div>
</nav>
   
    <div class="col-md-6 col-md-offset-3">
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cursos Cadastrados</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              
           
           <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th class="col-md-8 "><?= $this->Paginator->sort('nome') ?></th>
                <th class="actions col-md-4"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cursos as $curso): ?>
            <tr>
                 <td><?= h($curso->nome) ?></td>
                <td class="actions tamanhoFonte text-" style="white-space:nowrap;">
                    
                    <?= $this->Html->link(__('    '), ['action' => 'view', $curso->cursos_id], ['class'=>'fa fa-eye col-md-1  col-md-offset-2 ', 'title'=>'Detalhe']) ?>
                    <?= $this->Html->link(__('   '), ['action' => 'edit', $curso->cursos_id], ['class'=>'fa fa-pencil col-md-1 text-yellow', 'title'=>'Editar']) ?>
                    <?= $this->Form->postLink(__('   '), ['action' => 'delete', $curso->cursos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $curso->cursos_id), 'class'=>'fa fa-remove col-md-1 text-red', 'title'=>'Remover']) ?>
                 
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
                 <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('Anterior'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('Proxímo') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p class="corContador"><?= $this->Paginator->counter(__('Pagina {{page}} de {{pages}}, Mostrando {{current}} resultados de um total de
         {{count}}')) ?></p>
        </div>
                 </div>
          </div>

</div>
</div>