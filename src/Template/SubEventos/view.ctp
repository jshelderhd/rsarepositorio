<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Sub Evento']), ['action' => 'edit', $subEvento->sub_eventos_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Sub Evento']), ['action' => 'delete', $subEvento->sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEvento->sub_eventos_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subEventos view col-lg-10 col-md-9">
    <h3><?= h($subEvento->sub_eventos_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Sub Evento</th>
            <td><?= $subEvento->has('sub_evento') ? $this->Html->link($subEvento->sub_evento->sub_eventos_id, ['controller' => 'SubEventos', 'action' => 'view', $subEvento->sub_evento->sub_eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Nome</th>
            <td><?= h($subEvento->nome) ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $subEvento->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="related">
        <h4><?= __('Related {0}', ['Eventos']) ?></h4>
        <?php if (!empty($subEvento->eventos)): ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Eventos Id</th>
                <th>Titulo</th>
                <th>Apresentacao</th>
                <th>Edital</th>
                <th>Template</th>
                <th>Data Ini</th>
                <th>Data Fim</th>
                <th>Foto Evento</th>
                <th>Programacao</th>
                <th>Inf Horario</th>
                <th>Trabalho</th>
                <th>Ativo</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($subEvento->eventos as $eventos): ?>
            <tr>
                <td><?= h($eventos->eventos_id) ?></td>
                <td><?= h($eventos->titulo) ?></td>
                <td><?= h($eventos->apresentacao) ?></td>
                <td><?= h($eventos->edital) ?></td>
                <td><?= h($eventos->template) ?></td>
                <td><?= h($eventos->data_ini) ?></td>
                <td><?= h($eventos->data_fim) ?></td>
                <td><?= h($eventos->foto_evento) ?></td>
                <td><?= h($eventos->programacao) ?></td>
                <td><?= h($eventos->inf_horario) ?></td>
                <td><?= h($eventos->trabalho) ?></td>
                <td><?= h($eventos->ativo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Eventos', 'action' => 'view', $eventos->eventos_id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Eventos', 'action' => 'edit', $eventos->eventos_id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Eventos', 'action' => 'delete', $eventos->eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $eventos->eventos_id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related {0}', ['Inscricoes']) ?></h4>
        <?php if (!empty($subEvento->inscricoes)): ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Inscricoes Id</th>
                <th>Eventos Id</th>
                <th>Usuarios Id</th>
                <th>Tipo Insc</th>
                <th>Data Inscricao</th>
                <th>Autor Inscricoes Id</th>
                <th>Ativo</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($subEvento->inscricoes as $inscricoes): ?>
            <tr>
                <td><?= h($inscricoes->inscricoes_id) ?></td>
                <td><?= h($inscricoes->eventos_id) ?></td>
                <td><?= h($inscricoes->usuarios_id) ?></td>
                <td><?= h($inscricoes->tipo_insc) ?></td>
                <td><?= h($inscricoes->data_inscricao) ?></td>
                <td><?= h($inscricoes->autor_inscricoes_id) ?></td>
                <td><?= h($inscricoes->ativo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Inscricoes', 'action' => 'view', $inscricoes->inscricoes_id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Inscricoes', 'action' => 'edit', $inscricoes->inscricoes_id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Inscricoes', 'action' => 'delete', $inscricoes->inscricoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscricoes->inscricoes_id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
