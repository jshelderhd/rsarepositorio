<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subEventos index col-md-10 columns content">
    <h3>Sub Eventos</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('sub_eventos_id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subEventos as $subEvento): ?>
            <tr>
                <td><?= $subEvento->has('sub_evento') ? $this->Html->link($subEvento->sub_evento->sub_eventos_id, ['controller' => 'SubEventos', 'action' => 'view', $subEvento->sub_evento->sub_eventos_id]) : '' ?></td>
                <td><?= h($subEvento->nome) ?></td>
                <td><?= h($subEvento->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $subEvento->sub_eventos_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $subEvento->sub_eventos_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $subEvento->sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEvento->sub_eventos_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>