<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos'), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Sub Evento'), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos'), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Evento'), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Inscricoes'), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Inscrico'), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subEventos form col-md-10 columns content">
    <?= $this->Form->create($subEvento) ?>
    <fieldset>
        <legend><?= 'Add Sub Evento' ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('ativo');
            echo $this->Form->input('eventos._ids', ['options' => $eventos]);
            echo $this->Form->input('inscricoes._ids', ['options' => $inscricoes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
