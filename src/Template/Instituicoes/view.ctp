<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Instituico']), ['action' => 'edit', $instituico->instituicoes_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Instituico']), ['action' => 'delete', $instituico->instituicoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $instituico->instituicoes_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Instituicoes']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Instituico']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Instituicoes']), ['controller' => 'Instituicoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Instituico']), ['controller' => 'Instituicoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Cursos']), ['controller' => 'Cursos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Curso']), ['controller' => 'Cursos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="instituicoes view col-lg-10 col-md-9">
    <h3><?= h($instituico->instituicoes_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Instituico</th>
            <td><?= $instituico->has('instituico') ? $this->Html->link($instituico->instituico->instituicoes_id, ['controller' => 'Instituicoes', 'action' => 'view', $instituico->instituico->instituicoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Nome</th>
            <td><?= h($instituico->nome) ?></td>
        </tr>
        <tr>
            <th>Curso</th>
            <td><?= $instituico->has('curso') ? $this->Html->link($instituico->curso->cursos_id, ['controller' => 'Cursos', 'action' => 'view', $instituico->curso->cursos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $instituico->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
