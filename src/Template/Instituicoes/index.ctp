<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Instituico']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Cursos']), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Curso']), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="instituicoes index col-md-10 columns content">
    <h3>Instituicoes</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('instituicoes_id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('cursos_id') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($instituicoes as $instituico): ?>
            <tr>
                <td><?= $instituico->has('instituico') ? $this->Html->link($instituico->instituico->instituicoes_id, ['controller' => 'Instituicoes', 'action' => 'view', $instituico->instituico->instituicoes_id]) : '' ?></td>
                <td><?= h($instituico->nome) ?></td>
                <td><?= $instituico->has('curso') ? $this->Html->link($instituico->curso->cursos_id, ['controller' => 'Cursos', 'action' => 'view', $instituico->curso->cursos_id]) : '' ?></td>
                <td><?= h($instituico->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $instituico->instituicoes_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $instituico->instituicoes_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $instituico->instituicoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $instituico->instituicoes_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>