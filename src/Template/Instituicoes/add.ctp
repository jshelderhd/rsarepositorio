<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Instituicoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Instituicoes'), ['controller' => 'Instituicoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Instituico'), ['controller' => 'Instituicoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Cursos'), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Curso'), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="instituicoes form col-md-10 columns content">
    <?= $this->Form->create($instituico) ?>
    <fieldset>
        <legend><?= 'Add Instituico' ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('cursos_id', ['options' => $cursos]);
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
