   

    <div class="row">

        <nav class="navbar navbar-default" style="padding-left: 10px; padding-right: 10px; background:#64a7ce; color: white">
       <div class="container-fluid">
         <div class="navbar-header">
             <a style="color: white;"class="brand navbar-brand center-block " href="#">Usuários</a>
         </div>
           <ul class="nav navbar-nav navbar-right" style="padding-right: 10px;">
           
             <li style="font-size: 20px;"><?=$this->Html->link("<i class='fa fa-plus-circle' title='Adicionar Usuário'></i>", ['action'=>'add'], ['escape' => false]) ?></li>
         </ul>
       </div>
    </nav>
    <div class="users index col-md-12">
       <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cursos Cadastrados </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
        <table class="table table-striped table-hover  table-bordered">
            <thead>
                <tr>
                    
                    <th class="col-md-3"><?= $this->Paginator->sort('nome_completo') ?></th>
                    <th class="col-md-1"><?= $this->Paginator->sort('sexo') ?></th>
                    <th class="col-md-2"><?= $this->Paginator->sort('cpf') ?></th>
                    <th class="col-md-2"><?= $this->Paginator->sort('username') ?></th>
                    <th class="col-md-2"><?= $this->Paginator->sort('telefone') ?></th>
                    <th class="actions col-md-2"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                   
                    <td><?= h($user->nome_completo) ?></td>
                    <td><center><?php if($user->sexo=="m"){ echo "<i title='masculino' class='fa fa-mars ' style='color: blue;'></i>"; }
                    else{ echo "<i title='feminino' class='fa fa-venus text-danger'  style=''></i>"; } ?></center></td>
                    <td><?= h($user->cpf) ?></td> 
                    <td><?= h($user->username) ?></td>
                    <td><?= h($user->telefone) ?></td>
                    
                    <td class="actions tamanhoFonte text-" style="white-space:nowrap;">
                    
                    <?= $this->Html->link(__('    '), ['action' => 'view', $user->usuarios_id], ['class'=>'fa fa-eye col-md-1  col-md-offset-2 ', 'title'=>'Detalhe']) ?>
                    <?= $this->Html->link(__('   '), ['action' => 'edit', $user->usuarios_id], ['class'=>'fa fa-pencil col-md-1 text-yellow', 'title'=>'Editar']) ?>
                    <?= $this->Form->postLink(__('   '), ['action' => 'delete', $user->usuarios_id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->usuarios_id), 'class'=>'fa fa-remove col-md-1 text-red', 'title'=>'Remover']) ?>
                 
                </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <center>
                <ul class="pagination">
                    <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                    <?= $this->Paginator->numbers(['escape'=>false]) ?>
                    <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
             {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
           </center>
            </div>
        </div>
        </div>
        </div>
        </div>

  
