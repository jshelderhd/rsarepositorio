<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->usuarios_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->usuarios_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Instituicoes'), ['controller' => 'Instituicoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Instituico'), ['controller' => 'Instituicoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form col-md-10 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= 'Edit User' ?></legend>
        <?php
            echo $this->Form->input('nome_completo');
            echo $this->Form->input('sexo');
            echo $this->Form->input('cpf');
            echo $this->Form->input('username');
            echo $this->Form->input('telefone');
            echo $this->Form->input('password');
            echo $this->Form->input('data_criacao');
            echo $this->Form->input('instituicoes_id', ['options' => $instituicoes]);
            echo $this->Form->input('tipo_acesso');
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
