<nav class="navbar navbar-default" style="padding-left: 10px; padding-right: 10px; background:#64a7ce; color: white">
   <div class="container-fluid">
     <div class="navbar-header">
         <a style="color: white;"class="brand navbar-brand center-block " href="#">Usuários</a>
     </div>
       <ul class="nav navbar-nav navbar-right" style="padding-right: 10px;">
       
         <li style="font-size: 20px;"><?=$this->Html->link("<i class='fa fa-list-ul' title='Listar Usuários'></i>", ['action'=>'index'], ['escape' => false]) ?></li>
     </ul>
   </div>
</nav>
   
    <div class="container" >
    <div class="row">
            <div class="col-md-8 col-md-offset-2">

              <div class="box box-primary" style="margin-top: 20px;">
                <div class="box-header">
                  <h3 class="box-title">Cadastro de Usuários</h3>
                </div>
                <div class="box-body form-group">
                  <!-- Date dd/mm/yyyy -->
                   <?= $this->Form->create($user) ?>
                  <div class="form-group col-md-12">
                    <label>Nome:</label>
                  
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-vcard"></i>
                      </div>
                     <?php echo $this->Form->input('nome_completo',['label'=>false, 'placeholder'=>'Nome Completo']);?>
                    </div>
                    <!-- /.input group -->
                  </div>

                  <div class="form-group col-md-6 nopadding" >
                  <label>CPF</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-credit-card"></i>
                      </div>
                      <?php echo $this->Form->input('cpf',['label'=>false, 'placeholder'=>'CPF']); ?>
                    </div>
                    <!-- /.input group -->
                  </div>

                  <div class="form-group col-md-6 nopadding">
                    <label>Sexo:</label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-intersex"></i>
                      </div>
                     <?php echo $this->Form->select('sexo',['m'=>'MASCULINO','f'=>'FEMININO'],['class'=>'input-group','label'=>false]);?>
                    </div>
                    </div>
                    <div class="form-group col-md-6 nopadding" >
                  <label>Telefone</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <?php echo $this->Form->input('telefone',['label'=>false, 'placeholder'=>'Telefone']); ?>
                    </div>
                    <!-- /.input group -->
                  </div>

                  <div class="form-group col-md-6 nopadding" >
                    <label>Instituição:</label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-university"></i>
                      </div>
                     <?php echo $this->Form->select('instituicoes_id',$instituicoes,['label'=>false]);?>
                    </div>
                    <!-- /.input group -->
                  </div>
                  <hr>
                   <div class="form-group col-md-12">
                    <label>E-mail:</label>
                  
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-at"></i>
                      </div>
                     <?php echo $this->Form->input('username',['label'=>false, 'placeholder'=>'E-mail']);?>
                    </div>
                    <!-- /.input group -->
                  </div>
                   <div class="form-group col-md-12">
                    <label>Senha:</label>
                  
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-unlock-alt"></i>
                      </div>
                     <?php echo $this->Form->input('password',['label'=>false, 'placeholder'=>'Senha']);?>
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class="form-group col-md-12" >
                    <div class="input-group pull-right col-md-4">
                      <?= $this->Form->button(__('Salvar'),['class'=>'col-md-12']) ?>
                  </div>
                  
                      </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
    </div>
    </div>
    </div>

