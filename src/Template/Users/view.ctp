<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['User']), ['action' => 'edit', $user->usuarios_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['User']), ['action' => 'delete', $user->usuarios_id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->usuarios_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Users']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['User']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Instituicoes']), ['controller' => 'Instituicoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Instituico']), ['controller' => 'Instituicoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view col-lg-10 col-md-9">
    <h3><?= h($user->usuarios_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Nome Completo</th>
            <td><?= h($user->nome_completo) ?></td>
        </tr>
        <tr>
            <th>Sexo</th>
            <td><?= h($user->sexo) ?></td>
        </tr>
        <tr>
            <th>Cpf</th>
            <td><?= h($user->cpf) ?></td>
        </tr>
        <tr>
            <th>Username</th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th>Telefone</th>
            <td><?= h($user->telefone) ?></td>
        </tr>
        <tr>
            <th>Password</th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th>Instituico</th>
            <td><?= $user->has('instituico') ? $this->Html->link($user->instituico->instituicoes_id, ['controller' => 'Instituicoes', 'action' => 'view', $user->instituico->instituicoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>'Usuarios Id</th>
            <td><?= $this->Number->format($user->usuarios_id) ?></td>
        </tr>
        <tr>
            <th>'Tipo Acesso</th>
            <td><?= $this->Number->format($user->tipo_acesso) ?></td>
        </tr>
        <tr>
            <th>Data Criacao</th>
            <td><?= h($user->data_criacao) ?></tr>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $user->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
