<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ministrante'), ['action' => 'edit', $ministrante->ministrante_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ministrante'), ['action' => 'delete', $ministrante->ministrante_id], ['confirm' => __('Are you sure you want to delete # {0}?', $ministrante->ministrante_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ministrantes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ministrante'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ministrantes'), ['controller' => 'Ministrantes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ministrante'), ['controller' => 'Ministrantes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Eventos Sub Eventos'), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Eventos Sub Evento'), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ministrantes view large-9 medium-8 columns content">
    <h3><?= h($ministrante->ministrante_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ministrante') ?></th>
            <td><?= $ministrante->has('ministrante') ? $this->Html->link($ministrante->ministrante->ministrante_id, ['controller' => 'Ministrantes', 'action' => 'view', $ministrante->ministrante->ministrante_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome Ministrante') ?></th>
            <td><?= h($ministrante->nome_ministrante) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Foto Ministrante') ?></th>
            <td><?= h($ministrante->foto_ministrante) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Eventos Sub Evento') ?></th>
            <td><?= $ministrante->has('eventos_sub_evento') ? $this->Html->link($ministrante->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $ministrante->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Sobre') ?></h4>
        <?= $this->Text->autoParagraph(h($ministrante->sobre)); ?>
    </div>
</div>
