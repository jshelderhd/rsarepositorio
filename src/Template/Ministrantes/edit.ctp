<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ministrante->ministrante_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ministrante->ministrante_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ministrantes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ministrantes'), ['controller' => 'Ministrantes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ministrante'), ['controller' => 'Ministrantes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Eventos Sub Eventos'), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Eventos Sub Evento'), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ministrantes form large-9 medium-8 columns content">
    <?= $this->Form->create($ministrante) ?>
    <fieldset>
        <legend><?= __('Edit Ministrante') ?></legend>
        <?php
            echo $this->Form->control('nome_ministrante');
            echo $this->Form->control('foto_ministrante');
            echo $this->Form->control('sobre');
            echo $this->Form->control('Eventos_sub_eventos_id', ['options' => $eventosSubEventos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
