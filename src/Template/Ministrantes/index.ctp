<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ministrante'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Eventos Sub Eventos'), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Eventos Sub Evento'), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ministrantes index large-9 medium-8 columns content">
    <h3><?= __('Ministrantes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ministrante_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome_ministrante') ?></th>
                <th scope="col"><?= $this->Paginator->sort('foto_ministrante') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Eventos_sub_eventos_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ministrantes as $ministrante): ?>
            <tr>
                <td><?= $ministrante->has('ministrante') ? $this->Html->link($ministrante->ministrante->ministrante_id, ['controller' => 'Ministrantes', 'action' => 'view', $ministrante->ministrante->ministrante_id]) : '' ?></td>
                <td><?= h($ministrante->nome_ministrante) ?></td>
                <td><?= h($ministrante->foto_ministrante) ?></td>
                <td><?= $ministrante->has('eventos_sub_evento') ? $this->Html->link($ministrante->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $ministrante->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ministrante->ministrante_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ministrante->ministrante_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ministrante->ministrante_id], ['confirm' => __('Are you sure you want to delete # {0}?', $ministrante->ministrante_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
