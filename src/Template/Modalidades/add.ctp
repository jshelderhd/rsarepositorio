<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Modalidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Modalidades'), ['controller' => 'Modalidades', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Modalidade'), ['controller' => 'Modalidades', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Submicoes'), ['controller' => 'Submicoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Submico'), ['controller' => 'Submicoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="modalidades form col-md-10 columns content">
    <?= $this->Form->create($modalidade) ?>
    <fieldset>
        <legend><?= 'Add Modalidade' ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('submicoes_id', ['options' => $submicoes]);
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
