<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Modalidade']), ['action' => 'edit', $modalidade->modalidades_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Modalidade']), ['action' => 'delete', $modalidade->modalidades_id], ['confirm' => __('Are you sure you want to delete # {0}?', $modalidade->modalidades_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Modalidades']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Modalidade']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Modalidades']), ['controller' => 'Modalidades', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Modalidade']), ['controller' => 'Modalidades', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Submicoes']), ['controller' => 'Submicoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Submico']), ['controller' => 'Submicoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="modalidades view col-lg-10 col-md-9">
    <h3><?= h($modalidade->modalidades_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Modalidade</th>
            <td><?= $modalidade->has('modalidade') ? $this->Html->link($modalidade->modalidade->modalidades_id, ['controller' => 'Modalidades', 'action' => 'view', $modalidade->modalidade->modalidades_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Nome</th>
            <td><?= h($modalidade->nome) ?></td>
        </tr>
        <tr>
            <th>Submico</th>
            <td><?= $modalidade->has('submico') ? $this->Html->link($modalidade->submico->submicoes_id, ['controller' => 'Submicoes', 'action' => 'view', $modalidade->submico->submicoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $modalidade->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
