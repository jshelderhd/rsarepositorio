<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Modalidade']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Submicoes']), ['controller' => 'Submicoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Submico']), ['controller' => 'Submicoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="modalidades index col-md-10 columns content">
    <h3>Modalidades</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('modalidades_id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('submicoes_id') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($modalidades as $modalidade): ?>
            <tr>
                <td><?= $modalidade->has('modalidade') ? $this->Html->link($modalidade->modalidade->modalidades_id, ['controller' => 'Modalidades', 'action' => 'view', $modalidade->modalidade->modalidades_id]) : '' ?></td>
                <td><?= h($modalidade->nome) ?></td>
                <td><?= $modalidade->has('submico') ? $this->Html->link($modalidade->submico->submicoes_id, ['controller' => 'Submicoes', 'action' => 'view', $modalidade->submico->submicoes_id]) : '' ?></td>
                <td><?= h($modalidade->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $modalidade->modalidades_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $modalidade->modalidades_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $modalidade->modalidades_id], ['confirm' => __('Are you sure you want to delete # {0}?', $modalidade->modalidades_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>