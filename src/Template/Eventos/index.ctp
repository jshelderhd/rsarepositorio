            
    <style type="text/css">
    
.crop {
    width: 100%;
    height: 150px;
    overflow: hidden;
}

.crop img {
    height: 300px;
    
    display: block;
    width: 100%;
    max-width: 1200px; /* corresponds to max height of 450px */
    margin: 0 auto;
}
.bg-image {
    position: relative;
    padding: 0;
    border:1px solid;
}
.bg-image img {
    display: block;
    width: 100%;
    margin: 0 auto;
}
.bg-image h3 {
    position: absolute;
    padding-top: 20px;
     padding-left: 20px;
    top: 0;
    color: white;
}
.bg-image h4 {
    padding-top: 50px;
    padding-left: 20px;
    position: absolute;
    top: 0;
    color: white;
}
.widget-user-image{
    padding-top: 30px;
}
    </style>

    <?php $dir_foto_evento="files/Eventos/foto_evento/";
         $dir_logo_evento="files/Eventos/logo/";
         $dir_edital_evento="files/Eventos/edital/";
         $dir_templete_evento="files/Eventos/templete/"; ?>
            <div class="row">
             <nav class="navbar navbar-default" style="padding-left: 10px; padding-right: 10px; background:#3c8dbc; color: white">
       <div class="container-fluid">
         <div class="navbar-header">
             <a style="color: white;"class="brand navbar-brand center-block " href="#">Eventos</a>
         </div>
           <ul class="nav navbar-nav navbar-right" style="padding-right: 10px;">
           
             <li style="font-size: 20px;"><?=$this->Html->link("<i class='fa fa-plus-circle' title='Adicionar Evento'></i>", ['action'=>'add'], ['escape' => false]) ?></li>
         </ul>
       </div>
    </nav>

            <div class="eventos index col-md-12">
                
             
                        <?php foreach ($eventos as $evento): ?>
                            
                            <div class="col-md-8 col-md-offset-2 " style="margin-top: 20px;">
                      <!-- Widget: user widget style 1 -->
                      <div class="box box-widget widget-user box box-info">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class=" bg-aqua-active box-header crop bg-image" >
                         <img src="<?=$dir_foto_evento.$evento->foto_evento?>"  />
                          <h3 class="widget-user-username"><strong><?=$evento->titulo ?></strong></h3>
                          <h4 class="widget-user-desc">de <?= $evento->data_ini ?> a <?= $evento->data_fim ?></h4>
                        </div>                      

                        <div class="widget-user-image">
                         
                          <img class="img-circle" src="<?=$dir_logo_evento.$evento->logo?>">
                        </div>
                        <div class="box-footer box-body">
                          <div class="row" >
                            <div class="col-sm-4 border-right">
                              <div class="description-block">
                              <h4><a href="<?= $dir_edital_evento.$evento->edital ?>"><i class="fa fa-file-text-o"></i> </a></h4>
                                <span class="description-text">EDITAL</span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                              <div class="description-block white ">
                                <h4><a href="<?= $dir_edital_evento.$evento->edital ?>"><i class="fa fa-share-alt"></i></a></h4>
                                <span class="description-text">COMPARTILHAR</span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                              <div class="description-block">
                                <h4 ><a href="<?= $dir_edital_evento.$evento->edital ?>"><i class="fa fa-edit"></i></a></h4>
                                <span class="description-text">INSCREVER-SE</span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                          </div>
                          <div>
                          <hr>
                          <p><?= $evento->apresentacao ?></p>
            <hr>
                          </div>
                          <div class="row">
                           <div class="col-md-8">
                               <?php if($evento->trabalho){
                                echo "<h6>COMPATÍVEL COM SUBMISSÃO DE TRABALHO</h6>";
                                }else{
                                 echo "<h6>NÃO É COMPATÍVEL COM SUBMISSÃO DE TRABALHO</h6>";
                                    } ?>
                           </div>   
                           <div class="col-md-4">
                               <?= $this->Html->link(__(' Mais Detalhes...'), ['action' => 'view', $evento->eventos_id], ['class'=>' fa fa-info-circle btn btn-info btn-xs pull-right']) ?>
                           </div>
                          </div>
                          
                        </div>
                      </div>
                      <!-- /.widget-user -->
                    </div>
                    
                        
                     <?php endforeach; ?>

             
              
            </div>
            </div>

