<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Evento']), ['action' => 'edit', $evento->eventos_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Evento']), ['action' => 'delete', $evento->eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $evento->eventos_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="eventos view col-lg-10 col-md-9">
    <h3><?= h($evento->eventos_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Evento</th>
            <td><?= $evento->has('evento') ? $this->Html->link($evento->evento->eventos_id, ['controller' => 'Eventos', 'action' => 'view', $evento->evento->eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Edital</th>
            <td><?= h($evento->edital) ?></td>
        </tr>
        <tr>
            <th>Template</th>
            <td><?= h($evento->template) ?></td>
        </tr>
        <tr>
            <th>Foto Evento</th>
            <td><?= h($evento->foto_evento) ?></td>
        </tr>
        <tr>
            <th>Programacao</th>
            <td><?= h($evento->programacao) ?></td>
        </tr>
        <tr>
            <th>Inf Horario</th>
            <td><?= h($evento->inf_horario) ?></td>
        </tr>
        <tr>
            <th>Logo</th>
            <td><?= h($evento->logo) ?></td>
        </tr>
        <tr>
            <th>Data Ini</th>
            <td><?= h($evento->data_ini) ?></tr>
        </tr>
        <tr>
            <th>Data Fim</th>
            <td><?= h($evento->data_fim) ?></tr>
        </tr>
        <tr>
            <th>Trabalho</th>
            <td><?= $evento->trabalho ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $evento->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="row">
        <h4>Titulo</h4>
        <?= $this->Text->autoParagraph(h($evento->titulo)); ?>
    </div>
    <div class="row">
        <h4>Apresentacao</h4>
        <?= $this->Text->autoParagraph(h($evento->apresentacao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related {0}', ['Sub Eventos']) ?></h4>
        <?php if (!empty($evento->sub_eventos)): ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Sub Eventos Id</th>
                <th>Nome</th>
                <th>Ativo</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($evento->sub_eventos as $subEventos): ?>
            <tr>
                <td><?= h($subEventos->sub_eventos_id) ?></td>
                <td><?= h($subEventos->nome) ?></td>
                <td><?= h($subEventos->ativo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SubEventos', 'action' => 'view', $subEventos->sub_eventos_id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'SubEventos', 'action' => 'edit', $subEventos->sub_eventos_id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SubEventos', 'action' => 'delete', $subEventos->sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEventos->sub_eventos_id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
