<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $evento->eventos_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $evento->eventos_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos'), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Evento'), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos'), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Sub Evento'), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="eventos form col-md-10 columns content">
    <?= $this->Form->create($evento) ?>
    <fieldset>
        <legend><?= 'Edit Evento' ?></legend>
        <?php
            echo $this->Form->input('titulo');
            echo $this->Form->input('apresentacao');
            echo $this->Form->input('edital');
            echo $this->Form->input('template');
            echo $this->Form->input('data_ini', ['empty' => true, 'default' => '']);
            echo $this->Form->input('data_fim', ['empty' => true, 'default' => '']);
            echo $this->Form->input('foto_evento');
            echo $this->Form->input('programacao');
            echo $this->Form->input('inf_horario');
            echo $this->Form->input('trabalho');
            echo $this->Form->input('ativo');
            echo $this->Form->input('logo');
            echo $this->Form->input('sub_eventos._ids', ['options' => $subEventos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
