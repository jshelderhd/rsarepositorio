    <nav class="navbar navbar-default" style="padding-left: 10px; padding-right: 10px; background:#64a7ce; color: white">
     <div class="container-fluid">
       <div class="navbar-header">
           <a style="color: white;"class="brand navbar-brand center-block " href="#">Eventos</a>
       </div>
       <ul class="nav navbar-nav navbar-right" style="padding-right: 10px;">

           <li style="font-size: 20px;"><?=$this->Html->link("<i class='fa fa-list-ul' title='Listar Eventos'></i>", ['action'=>'index'], ['escape' => false]) ?></li>
       </ul>
   </div>
</nav>
<script type="text/javascript">
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
            log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
            
        });
        function readURL(input, i) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    if(i==2){
                      $('#img-upload2').attr('src', e.target.result);  
                  }else{
                    $('#img-upload').attr('src', e.target.result); 
                }


            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this, 1);
    }); 
    $("#imgInp2").change(function(){
        readURL(this, 2);
    });    
});
    (function( $ ) {
    $.widget( "ui.dp", {
            _create: function() {
                var el = this.element.hide();
                this.options.altField = el;
                var input = this.input = $('<input>').insertBefore( el )
                input.focusout(function(){
                        if(input.val() == ''){
                            el.val('');
                        }
                    });
                input.datepicker(this.options)
                if(convertDate(el.val()) != null){
                    this.input.datepicker('setDate', convertDate(el.val()));
                }
            },
            destroy: function() {
                this.input.remove();
                this.element.show();
                $.Widget.prototype.destroy.call( this );
            }
    });
   
    var convertDate = function(date){
      if(typeof(date) != 'undefined' && date != null && date != ''){
        return new Date(date);
      } else {
        return null;
      }
    }
})( jQuery );

</script>
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 25%;
    }
    #img-upload2{
        width: 50%;
    }
    .onoffswitch {
        position: relative; width: 88px;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }

    .onoffswitch-checkbox {
        display: none;
    }

    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        height: 36px; padding: 0; line-height: 36px;
        border: 2px solid #E3E3E3; border-radius: 36px;
        background-color: #EDEDED;
        transition: background-color 0.3s ease-in;
    }

    .onoffswitch-label:before {
        content: "";
        display: block; width: 36px; margin: 0px;
        background: #FFFFFF;
        position: absolute; top: 0; bottom: 0;
        right: 50px;
        border: 2px solid #E3E3E3; border-radius: 36px;
        transition: all 0.3s ease-in 0s; 
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label {
        background-color: #00C0EF;
    }


    .onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
     border-color: #00C0EF;
 }

 .onoffswitch-checkbox:checked + .onoffswitch-label:before {
    right: 0px; 
}
.table td {
   text-align: center;   
}

</style>

<div class="container" >
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-primary" style="margin-top: 20px;">
            <div class="box-header">
              <h3 class="box-title">Cadastro de Eventos</h3>
          </div>

          <div class="box-body form-group">
              <!-- Date dd/mm/yyyy -->
              <?= $this->Form->create($evento, ['type' => 'file']) ;
              $this->Form->templates([
    'dateWidget' => '{{day}}{{month}}{{year}}{{hour}}{{minute}}{{second}}{{meridian}}'
]);?>

              <div class="form-group">

                  <div class="form-group col-md-12">

                    <label>Título:</label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-quote-right"></i>
                    </div>
                    <?php  echo $this->Form->input('titulo',['type'=>'text','label'=>false, 'placeholder'=>'Título do Evento']);?>
                </div>
                <!-- /.input group -->
            </div>
            <div class="form-group col-md-12">

                <label>Apresentação:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-info-circle"></i>
                </div>
                <?php echo $this->Form->input('apresentacao',['label'=>false, 'placeholder'=>'Apresentação do Evento']);?>
            </div>
            <!-- /.input group -->
            
        </div>

        <div class="form-group col-md-12">
         <label>Edital:</label>
         <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-file-pdf-o"></i>
            </div>
            <input type="text" class="form-control" placeholder="Edital do Evento" readonly>
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Pesquisar… <input type="file"  name="edital">
                </span>
            </span>

        </div>

    
    </div>
    <div class="form-group col-md-12">
     <label>Template:</label>
     <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-file-word-o"></i>
        </div>
        <input type="text" class="form-control" placeholder="Template do Evento" readonly>
        <span class="input-group-btn">
            <span class="btn btn-default btn-file">
                Pesquisar… <input type="file"  name="template">
            </span>
        </span>
    </div>
</div>

<div class="form-group col-md-6 nopadding" >
  <label>Data Inicio</label>
  
    <div id="datetimepicker" class="input-group put-append date">
      <div  class="input-group-addon add-on" >
        <a href="#" class="fa fa-calendar"></a>
    </div>
      <?php echo $this->Form->input('data_ini', ['type'=>'text','label'=>false]);?>
    </div>

</div>
<div class="form-group col-md-6 nopadding" >
  <label>Data Inicio</label>
    <div id="datetimepicker2" class="input-group put-append date">
      <div class="input-group-addon add-on">
        <a href="#"  class="fa fa-calendar"></a>
    </div>
      <?php echo $this->Form->input('data_fim', ['type'=>'text','label'=>false]);?>
    </div>

</div>

<div class="form-group col-md-12">
 <label>Logo do Evento:</label>
 <div class="input-group">
    <div class="input-group-addon">
        <i class="fa fa-university"></i>
    </div>
    <input type="text" class="form-control" placeholder="LogoMarca do Evento 256x256" readonly>
    <span class="input-group-btn">
        <span class="btn btn-default btn-file">
            Pesquisar… <input type="file" id="imgInp" name="logo">
        </span>
    </span>

</div>
<img id='img-upload'/>
</div>
<div class="form-group col-md-12">
 <label>Logo do Evento:</label>
 <div class="input-group">
    <div class="input-group-addon">
        <i class="fa fa-university"></i>
    </div>
    <input type="text" class="form-control" placeholder="Foto do Evento 1024x860" readonly>
    <span class="input-group-btn">
        <span class="btn btn-default btn-file">
            Pesquisar… <input type="file" id="imgInp2" name="foto_evento">
        </span>
    </span>

</div>
<img id='img-upload2'/>
</div>


<div class="form-group col-md-12 nopadding" >
    <label>Programação:</label>

    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-university"></i>
    </div>
    <?php echo $this->Form->input('programacao',['label'=>false,'type'=>'textarea', 'placeholder'=>'Programação do Evento']);?>
</div>
<!-- /.input group -->
</div>
<hr>
<div class="form-group col-md-12">
    <label>Informações de Horários:</label>

    <div class="input-group">
      <div class="input-group-addon">
        <i class="fa fa-at"></i>
    </div>
    <?php echo $this->Form->input('inf_horario',['label'=>false, 'placeholder'=>'Informações sobre Horarios do Evento']);?>
</div>
<!-- /.input group -->
</div>

<div class="form-group col-md-12">
    <label>Submição de Trabalho:</label>

    <div class="input-group">

      <div class="onoffswitch">
          <input type="checkbox" name="trabalho" class="onoffswitch-checkbox" value="1" id="myonoffswitch" checked>
          <label class="onoffswitch-label" for="myonoffswitch"></label>
      </div>

  </div>
  <!-- /.input group -->
</div>
<div class="form-group col-md-12" >
    <div class="input-group pull-right col-md-4">
      <?= $this->Form->button(__('Salvar'),['class'=>'col-md-12']) ?>
  </div>

</div>
</div>
<table class="table table-striped table-hover table-bordered text-align center">
        <thead>
       
            <tr>
                <th class="col-md-2 ">Horário</th>
                <th class="actions col-md-10">Acontecimento</th>
            
        </thead>
        
        <tbody>
             </tr>
            <tr><th colspan="2" class="actions col-md-12">12/08/1993</th></tr>
             <tr>
                 <td>08:20</td><td> NOME NO ACONTECIMENTO QUE VAI OCORRER</td>
                
            </tr>
            <tr>
                 <td>09:20</td><td> NOME NO ACONTECIMENTO QUE VAI OCORRER</td>
                
            </tr>
            <tr>
                 <td>10:20</td><td> NOME NO ACONTECIMENTO QUE VAI OCORRER</td>
                
            </tr>
              </tr>
            <tr><th colspan="2" class="actions col-md-12">13/08/1993</th></tr>
             <tr>
                 <td>08:20</td><td> NOME NO ACONTECIMENTO QUE VAI OCORRER</td>
                
            </tr>
            <tr>
                 <td>09:20</td><td> NOME NO ACONTECIMENTO QUE VAI OCORRER</td>
                
            </tr>
            <tr>
                 <td>10:20</td><td> NOME NO ACONTECIMENTO QUE VAI OCORRER</td>
                
            </tr>
          
        </tbody>
        
    </table>
</div>
</div>
<!-- /.box -->
</div>

</div>
 

<script type="text/javascript">
      $('#datetimepicker').datetimepicker({
        format: 'dd/MM/yyyy',
        language: 'pt-BR',
        pickTime: false
      });
      $('#datetimepicker2').datetimepicker({
        format: 'dd/MM/yyyy',
        language: 'pt-BR',
        pickTime: false
      });
    </script>