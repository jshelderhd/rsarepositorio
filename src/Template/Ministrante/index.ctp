<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Ministrante']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos Sub Eventos']), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ministrante index col-md-10 columns content">
    <h3>Ministrante</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('ministrante_id') ?></th>
                <th><?= $this->Paginator->sort('nome_ministrante') ?></th>
                <th><?= $this->Paginator->sort('foto_ministrante') ?></th>
                <th><?= $this->Paginator->sort('Eventos_sub_eventos_id') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ministrante as $ministrante): ?>
            <tr>
                <td><?= $this->Number->format($ministrante->ministrante_id) ?></td>
                <td><?= h($ministrante->nome_ministrante) ?></td>
                <td><?= h($ministrante->foto_ministrante) ?></td>
                <td><?= $ministrante->has('eventos_sub_evento') ? $this->Html->link($ministrante->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $ministrante->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
                <td><?= h($ministrante->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ministrante->ministrante_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ministrante->ministrante_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ministrante->ministrante_id], ['confirm' => __('Are you sure you want to delete # {0}?', $ministrante->ministrante_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>