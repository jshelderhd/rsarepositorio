<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Ministrante']), ['action' => 'edit', $ministrante->ministrante_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Ministrante']), ['action' => 'delete', $ministrante->ministrante_id], ['confirm' => __('Are you sure you want to delete # {0}?', $ministrante->ministrante_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Ministrante']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Ministrante']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos Sub Eventos']), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Eventos Sub Evento']), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ministrante view col-lg-10 col-md-9">
    <h3><?= h($ministrante->ministrante_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Nome Ministrante</th>
            <td><?= h($ministrante->nome_ministrante) ?></td>
        </tr>
        <tr>
            <th>Foto Ministrante</th>
            <td><?= h($ministrante->foto_ministrante) ?></td>
        </tr>
        <tr>
            <th>Eventos Sub Evento</th>
            <td><?= $ministrante->has('eventos_sub_evento') ? $this->Html->link($ministrante->eventos_sub_evento->eventos_sub_eventos_id, ['controller' => 'EventosSubEventos', 'action' => 'view', $ministrante->eventos_sub_evento->eventos_sub_eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>'Ministrante Id</th>
            <td><?= $this->Number->format($ministrante->ministrante_id) ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $ministrante->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="row">
        <h4>Sobre</h4>
        <?= $this->Text->autoParagraph(h($ministrante->sobre)); ?>
    </div>
</div>
