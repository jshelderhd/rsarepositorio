<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ministrante->ministrante_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ministrante->ministrante_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Ministrante'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos Sub Eventos'), ['controller' => 'EventosSubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Eventos Sub Evento'), ['controller' => 'EventosSubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ministrante form col-md-10 columns content">
    <?= $this->Form->create($ministrante) ?>
    <fieldset>
        <legend><?= 'Edit Ministrante' ?></legend>
        <?php
            echo $this->Form->input('nome_ministrante');
            echo $this->Form->input('foto_ministrante');
            echo $this->Form->input('sobre');
            echo $this->Form->input('Eventos_sub_eventos_id', ['options' => $eventosSubEventos]);
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
