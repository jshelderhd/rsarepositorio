<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Usuarios']), ['controller' => 'Usuarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Usuario']), ['controller' => 'Usuarios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inscricoes index col-md-10 columns content">
    <h3>Inscricoes</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('inscricoes_id') ?></th>
                <th><?= $this->Paginator->sort('eventos_id') ?></th>
                <th><?= $this->Paginator->sort('usuarios_id') ?></th>
                <th><?= $this->Paginator->sort('tipo_insc') ?></th>
                <th><?= $this->Paginator->sort('data_inscricao') ?></th>
                <th><?= $this->Paginator->sort('autor_inscricoes_id') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($inscricoes as $inscrico): ?>
            <tr>
                <td><?= $this->Number->format($inscrico->inscricoes_id) ?></td>
                <td><?= $inscrico->has('evento') ? $this->Html->link($inscrico->evento->eventos_id, ['controller' => 'Eventos', 'action' => 'view', $inscrico->evento->eventos_id]) : '' ?></td>
                <td><?= $inscrico->has('usuario') ? $this->Html->link($inscrico->usuario->usuarios_id, ['controller' => 'Usuarios', 'action' => 'view', $inscrico->usuario->usuarios_id]) : '' ?></td>
                <td><?= h($inscrico->tipo_insc) ?></td>
                <td><?= h($inscrico->data_inscricao) ?></td>
                <td><?= $inscrico->has('inscrico') ? $this->Html->link($inscrico->inscrico->inscricoes_id, ['controller' => 'Inscricoes', 'action' => 'view', $inscrico->inscrico->inscricoes_id]) : '' ?></td>
                <td><?= h($inscrico->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $inscrico->inscricoes_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $inscrico->inscricoes_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $inscrico->inscricoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscrico->inscricoes_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>