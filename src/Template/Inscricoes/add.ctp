<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Inscricoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Inscricoes'), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Inscrico'), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Eventos'), ['controller' => 'Eventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Evento'), ['controller' => 'Eventos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Sub Eventos'), ['controller' => 'SubEventos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Sub Evento'), ['controller' => 'SubEventos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inscricoes form col-md-10 columns content">
    <?= $this->Form->create($inscrico) ?>
    <fieldset>
        <legend><?= 'Add Inscrico' ?></legend>
        <?php
            echo $this->Form->input('eventos_id', ['options' => $eventos]);
            echo $this->Form->input('usuarios_id', ['options' => $usuarios]);
            echo $this->Form->input('tipo_insc');
            echo $this->Form->input('data_inscricao');
            echo $this->Form->input('autor_inscricoes_id', ['options' => $inscricoes]);
            echo $this->Form->input('ativo');
            echo $this->Form->input('sub_eventos._ids', ['options' => $subEventos]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
