<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Inscrico']), ['action' => 'edit', $inscrico->inscricoes_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Inscrico']), ['action' => 'delete', $inscrico->inscricoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscrico->inscricoes_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Eventos']), ['controller' => 'Eventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Evento']), ['controller' => 'Eventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Usuarios']), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Usuario']), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Sub Eventos']), ['controller' => 'SubEventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Sub Evento']), ['controller' => 'SubEventos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="inscricoes view col-lg-10 col-md-9">
    <h3><?= h($inscrico->inscricoes_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Evento</th>
            <td><?= $inscrico->has('evento') ? $this->Html->link($inscrico->evento->eventos_id, ['controller' => 'Eventos', 'action' => 'view', $inscrico->evento->eventos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Usuario</th>
            <td><?= $inscrico->has('usuario') ? $this->Html->link($inscrico->usuario->usuarios_id, ['controller' => 'Usuarios', 'action' => 'view', $inscrico->usuario->usuarios_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Inscrico</th>
            <td><?= $inscrico->has('inscrico') ? $this->Html->link($inscrico->inscrico->inscricoes_id, ['controller' => 'Inscricoes', 'action' => 'view', $inscrico->inscrico->inscricoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>'Inscricoes Id</th>
            <td><?= $this->Number->format($inscrico->inscricoes_id) ?></td>
        </tr>
        <tr>
            <th>Data Inscricao</th>
            <td><?= h($inscrico->data_inscricao) ?></tr>
        </tr>
        <tr>
            <th>Tipo Insc</th>
            <td><?= $inscrico->tipo_insc ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $inscrico->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="related">
        <h4><?= __('Related {0}', ['Sub Eventos']) ?></h4>
        <?php if (!empty($inscrico->sub_eventos)): ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Sub Eventos Id</th>
                <th>Nome</th>
                <th>Ativo</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($inscrico->sub_eventos as $subEventos): ?>
            <tr>
                <td><?= h($subEventos->sub_eventos_id) ?></td>
                <td><?= h($subEventos->nome) ?></td>
                <td><?= h($subEventos->ativo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SubEventos', 'action' => 'view', $subEventos->sub_eventos_id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'SubEventos', 'action' => 'edit', $subEventos->sub_eventos_id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SubEventos', 'action' => 'delete', $subEventos->sub_eventos_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subEventos->sub_eventos_id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
