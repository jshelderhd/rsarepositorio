<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Usuario']), ['action' => 'edit', $usuario->usuarios_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Usuario']), ['action' => 'delete', $usuario->usuarios_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->usuarios_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Usuarios']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Usuario']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Usuarios']), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Usuario']), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Instituicoes']), ['controller' => 'Instituicoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Instituico']), ['controller' => 'Instituicoes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usuarios view col-lg-10 col-md-9">
    <h3><?= h($usuario->usuarios_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Usuario</th>
            <td><?= $usuario->has('usuario') ? $this->Html->link($usuario->usuario->usuarios_id, ['controller' => 'Usuarios', 'action' => 'view', $usuario->usuario->usuarios_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Nome Completo</th>
            <td><?= h($usuario->nome_completo) ?></td>
        </tr>
        <tr>
            <th>Sexo</th>
            <td><?= h($usuario->sexo) ?></td>
        </tr>
        <tr>
            <th>Cpf</th>
            <td><?= h($usuario->cpf) ?></td>
        </tr>
        <tr>
            <th>Username</th>
            <td><?= h($usuario->username) ?></td>
        </tr>
        <tr>
            <th>Telefone</th>
            <td><?= h($usuario->telefone) ?></td>
        </tr>
        <tr>
            <th>Password</th>
            <td><?= h($usuario->password) ?></td>
        </tr>
        <tr>
            <th>Instituico</th>
            <td><?= $usuario->has('instituico') ? $this->Html->link($usuario->instituico->instituicoes_id, ['controller' => 'Instituicoes', 'action' => 'view', $usuario->instituico->instituicoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>'Tipo Acesso</th>
            <td><?= $this->Number->format($usuario->tipo_acesso) ?></td>
        </tr>
        <tr>
            <th>Data Criacao</th>
            <td><?= h($usuario->data_criacao) ?></tr>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $usuario->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
</div>
