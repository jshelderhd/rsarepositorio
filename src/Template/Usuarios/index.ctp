<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Usuario']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Instituicoes']), ['controller' => 'Instituicoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Instituico']), ['controller' => 'Instituicoes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usuarios index col-md-10 columns content">
    <h3>Usuarios</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('usuarios_id') ?></th>
                <th><?= $this->Paginator->sort('nome_completo') ?></th>
                <th><?= $this->Paginator->sort('sexo') ?></th>
                <th><?= $this->Paginator->sort('cpf') ?></th>
                <th><?= $this->Paginator->sort('username') ?></th>
                <th><?= $this->Paginator->sort('telefone') ?></th>
                <th><?= $this->Paginator->sort('password') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?= $usuario->has('usuario') ? $this->Html->link($usuario->usuario->usuarios_id, ['controller' => 'Usuarios', 'action' => 'view', $usuario->usuario->usuarios_id]) : '' ?></td>
                <td><?= h($usuario->nome_completo) ?></td>
                <td><?= h($usuario->sexo) ?></td>
                <td><?= h($usuario->cpf) ?></td>
                <td><?= h($usuario->username) ?></td>
                <td><?= h($usuario->telefone) ?></td>
                <td><?= h($usuario->password) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usuario->usuarios_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usuario->usuarios_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usuario->usuarios_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->usuarios_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>