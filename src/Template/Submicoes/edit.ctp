<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $submico->submicoes_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $submico->submicoes_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Submicoes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Submicoes'), ['controller' => 'Submicoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Submico'), ['controller' => 'Submicoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Inscricoes'), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Inscrico'), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', 'Cursos'), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', 'Curso'), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="submicoes form col-md-10 columns content">
    <?= $this->Form->create($submico) ?>
    <fieldset>
        <legend><?= 'Edit Submico' ?></legend>
        <?php
            echo $this->Form->input('titulo');
            echo $this->Form->input('inscricoes_id', ['options' => $inscricoes]);
            echo $this->Form->input('cursos_id', ['options' => $cursos]);
            echo $this->Form->input('ativo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
