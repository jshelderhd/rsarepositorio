<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Submico']), ['action' => 'edit', $submico->submicoes_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Submico']), ['action' => 'delete', $submico->submicoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $submico->submicoes_id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Submicoes']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Submico']), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Submicoes']), ['controller' => 'Submicoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Submico']), ['controller' => 'Submicoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Cursos']), ['controller' => 'Cursos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Curso']), ['controller' => 'Cursos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="submicoes view col-lg-10 col-md-9">
    <h3><?= h($submico->submicoes_id) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Submico</th>
            <td><?= $submico->has('submico') ? $this->Html->link($submico->submico->submicoes_id, ['controller' => 'Submicoes', 'action' => 'view', $submico->submico->submicoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Inscrico</th>
            <td><?= $submico->has('inscrico') ? $this->Html->link($submico->inscrico->inscricoes_id, ['controller' => 'Inscricoes', 'action' => 'view', $submico->inscrico->inscricoes_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Curso</th>
            <td><?= $submico->has('curso') ? $this->Html->link($submico->curso->cursos_id, ['controller' => 'Cursos', 'action' => 'view', $submico->curso->cursos_id]) : '' ?></td>
        </tr>
        <tr>
            <th>Ativo</th>
            <td><?= $submico->ativo ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="row">
        <h4>Titulo</h4>
        <?= $this->Text->autoParagraph(h($submico->titulo)); ?>
    </div>
</div>
