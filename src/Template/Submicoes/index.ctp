<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Submico']), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Inscricoes']), ['controller' => 'Inscricoes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Inscrico']), ['controller' => 'Inscricoes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List {0}', ['Cursos']), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New {0}', ['Curso']), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="submicoes index col-md-10 columns content">
    <h3>Submicoes</h3>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('submicoes_id') ?></th>
                <th><?= $this->Paginator->sort('inscricoes_id') ?></th>
                <th><?= $this->Paginator->sort('cursos_id') ?></th>
                <th><?= $this->Paginator->sort('ativo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($submicoes as $submico): ?>
            <tr>
                <td><?= $submico->has('submico') ? $this->Html->link($submico->submico->submicoes_id, ['controller' => 'Submicoes', 'action' => 'view', $submico->submico->submicoes_id]) : '' ?></td>
                <td><?= $submico->has('inscrico') ? $this->Html->link($submico->inscrico->inscricoes_id, ['controller' => 'Inscricoes', 'action' => 'view', $submico->inscrico->inscricoes_id]) : '' ?></td>
                <td><?= $submico->has('curso') ? $this->Html->link($submico->curso->cursos_id, ['controller' => 'Cursos', 'action' => 'view', $submico->curso->cursos_id]) : '' ?></td>
                <td><?= h($submico->ativo) ?></td>
                <td class="actions" style="white-space:nowrap">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $submico->submicoes_id], ['class'=>'btn btn-default btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $submico->submicoes_id], ['class'=>'btn btn-primary btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $submico->submicoes_id], ['confirm' => __('Are you sure you want to delete # {0}?', $submico->submicoes_id), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <center>
            <ul class="pagination">
                <?= $this->Paginator->prev('&laquo; ' . __('previous'), ['escape'=>false]) ?>
                <?= $this->Paginator->numbers(['escape'=>false]) ?>
                <?= $this->Paginator->next(__('next') . ' &raquo;', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} records out of
         {{count}} total, starting on record {{start}}, ending on {{end}}')) ?></p>
        </div>
    </center>
</div>
</div>