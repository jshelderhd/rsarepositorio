<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InscricoesFixture
 *
 */
class InscricoesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'inscricoes_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'eventos_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'usuarios_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_insc' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'data_inscricao' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'autor_inscricoes_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ativo' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Eventos_has_Usuarios_Usuarios1_idx' => ['type' => 'index', 'columns' => ['usuarios_id'], 'length' => []],
            'fk_Eventos_has_Usuarios_Eventos_idx' => ['type' => 'index', 'columns' => ['eventos_id'], 'length' => []],
            'fk_Inscricoes_Inscricoes1_idx' => ['type' => 'index', 'columns' => ['autor_inscricoes_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['inscricoes_id'], 'length' => []],
            'fk_Eventos_has_Usuarios_Eventos' => ['type' => 'foreign', 'columns' => ['eventos_id'], 'references' => ['eventos', 'eventos_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Eventos_has_Usuarios_Usuarios1' => ['type' => 'foreign', 'columns' => ['usuarios_id'], 'references' => ['usuarios', 'usuarios_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Inscricoes_Inscricoes1' => ['type' => 'foreign', 'columns' => ['autor_inscricoes_id'], 'references' => ['inscricoes', 'inscricoes_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'inscricoes_id' => 1,
            'eventos_id' => 1,
            'usuarios_id' => 1,
            'tipo_insc' => 1,
            'data_inscricao' => '2017-03-20 18:48:04',
            'autor_inscricoes_id' => 1,
            'ativo' => 1
        ],
    ];
}
