<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubEventosInscricoesFixture
 *
 */
class SubEventosInscricoesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'evento_sub_eventos_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'inscricoes_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ativo' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Eventos_Sub_eventos_has_Inscricoes_Inscricoes1_idx' => ['type' => 'index', 'columns' => ['inscricoes_id'], 'length' => []],
            'fk_Eventos_Sub_eventos_has_Inscricoes_Eventos_Sub_eventos1_idx' => ['type' => 'index', 'columns' => ['evento_sub_eventos_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['evento_sub_eventos_id', 'inscricoes_id'], 'length' => []],
            'fk_Eventos_Sub_eventos_has_Inscricoes_Eventos_Sub_eventos1' => ['type' => 'foreign', 'columns' => ['evento_sub_eventos_id'], 'references' => ['eventos_sub_eventos', 'eventos_sub_eventos_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Eventos_Sub_eventos_has_Inscricoes_Inscricoes1' => ['type' => 'foreign', 'columns' => ['inscricoes_id'], 'references' => ['inscricoes', 'inscricoes_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'evento_sub_eventos_id' => 1,
            'inscricoes_id' => 1,
            'ativo' => 1
        ],
    ];
}
