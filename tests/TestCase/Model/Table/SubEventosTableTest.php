<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubEventosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubEventosTable Test Case
 */
class SubEventosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubEventosTable
     */
    public $SubEventos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sub_eventos',
        'app.eventos',
        'app.eventos_sub_eventos',
        'app.inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos',
        'app.sub_eventos_inscricoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SubEventos') ? [] : ['className' => 'App\Model\Table\SubEventosTable'];
        $this->SubEventos = TableRegistry::get('SubEventos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubEventos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
