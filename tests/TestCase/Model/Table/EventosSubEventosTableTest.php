<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EventosSubEventosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EventosSubEventosTable Test Case
 */
class EventosSubEventosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EventosSubEventosTable
     */
    public $EventosSubEventos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.eventos_sub_eventos',
        'app.eventos',
        'app.sub_eventos',
        'app.inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos',
        'app.sub_eventos_inscricoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EventosSubEventos') ? [] : ['className' => 'App\Model\Table\EventosSubEventosTable'];
        $this->EventosSubEventos = TableRegistry::get('EventosSubEventos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EventosSubEventos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
