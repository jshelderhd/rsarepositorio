<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MinistranteTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MinistranteTable Test Case
 */
class MinistranteTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MinistranteTable
     */
    public $Ministrante;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ministrante',
        'app.ministrantes',
        'app.eventos_sub_eventos',
        'app.eventos',
        'app.sub_eventos',
        'app.inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos',
        'app.sub_eventos_inscricoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ministrante') ? [] : ['className' => 'App\Model\Table\MinistranteTable'];
        $this->Ministrante = TableRegistry::get('Ministrante', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ministrante);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
