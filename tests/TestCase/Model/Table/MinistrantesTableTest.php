<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MinistrantesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MinistrantesTable Test Case
 */
class MinistrantesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MinistrantesTable
     */
    public $Ministrantes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ministrantes',
        'app.eventos_sub_eventos',
        'app.eventos',
        'app.sub_eventos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ministrantes') ? [] : ['className' => 'App\Model\Table\MinistrantesTable'];
        $this->Ministrantes = TableRegistry::get('Ministrantes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ministrantes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
