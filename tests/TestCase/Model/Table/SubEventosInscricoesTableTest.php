<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubEventosInscricoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubEventosInscricoesTable Test Case
 */
class SubEventosInscricoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubEventosInscricoesTable
     */
    public $SubEventosInscricoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sub_eventos_inscricoes',
        'app.eventos_sub_eventos',
        'app.eventos',
        'app.sub_eventos',
        'app.inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SubEventosInscricoes') ? [] : ['className' => 'App\Model\Table\SubEventosInscricoesTable'];
        $this->SubEventosInscricoes = TableRegistry::get('SubEventosInscricoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SubEventosInscricoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
