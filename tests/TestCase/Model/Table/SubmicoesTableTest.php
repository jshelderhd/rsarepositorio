<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubmicoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubmicoesTable Test Case
 */
class SubmicoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubmicoesTable
     */
    public $Submicoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.submicoes',
        'app.inscricoes',
        'app.eventos',
        'app.sub_eventos',
        'app.eventos_sub_eventos',
        'app.sub_eventos_inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Submicoes') ? [] : ['className' => 'App\Model\Table\SubmicoesTable'];
        $this->Submicoes = TableRegistry::get('Submicoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Submicoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
