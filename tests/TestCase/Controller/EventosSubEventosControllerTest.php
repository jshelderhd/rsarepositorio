<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EventosSubEventosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EventosSubEventosController Test Case
 */
class EventosSubEventosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.eventos_sub_eventos',
        'app.eventos',
        'app.sub_eventos',
        'app.inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos',
        'app.sub_eventos_inscricoes'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
