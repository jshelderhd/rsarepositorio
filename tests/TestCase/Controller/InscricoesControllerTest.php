<?php
namespace App\Test\TestCase\Controller;

use App\Controller\InscricoesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\InscricoesController Test Case
 */
class InscricoesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.inscricoes',
        'app.eventos',
        'app.sub_eventos',
        'app.eventos_sub_eventos',
        'app.sub_eventos_inscricoes',
        'app.usuarios',
        'app.instituicoes',
        'app.cursos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
